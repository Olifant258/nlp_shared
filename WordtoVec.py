"""
Created on Tue Mar 15 14:12:14 2016

@author: Mark
"""
import itertools
import os
import pickle
import math
from nltk.stem.porter import *
import collections
import time
import numpy as np
try:
    import gensim
except NameError:
    print('gensim not installed')
import heapq

stemmer = PorterStemmer()

from stemming.porter2 import stem

#Function to open the data from the file and return it as the rough data.
def open_file(filePath, pkl=True):
    #Get relative path to conference proceedings folder and select the file as a filename;
    fileDir = os.path.dirname(os.path.realpath('__file__'))
    #Open the file, read out its contents and then close the file;
    if pkl == True:
        #Get path to file.
        filename = os.path.join(fileDir, filePath + '.pkl')
        #Open the file, pkl format.
        file = open(filename, 'rb')
        #Load the data.
        text = pickle.load(file)
        file.close()
    else:
        filename = os.path.join(fileDir, filePath + '.txt')
        file = open(filename)
        text = file.read()
        file.close()
    #and return the text data.
    return text
    
#Function to pre-process all data (tokenize into words). Returns training and test data.
def process_data(text,n, m):
    
    #Split up in matrix for each \n and \t in the document. Remain only with the titles of the papers.
    text = [line.split('\t') for line in text.split('\n')]
    #Format the data, so that we remain only with a list of lists of tokenized sentences.
    dataid = []
    datacategory = []
    dataline = []
    lineMov = '.,!?:;()-/\+<>' #Special characters to be removed.
    for line in text:
        if len(line) > 1:
            id = line[0]
            category = line[1]
            line = line[2:]
            line = line[0].lower() #Make all letters low cases.
            for i in range(len(lineMov)): #Remove special characters and replace them with a white space.
                line = line.replace(lineMov[i], ' ')
            line = '<bos> ' + line + ' <eos>' #Add tokens for the start of the sentence and the end of the sentence.
            line = line.split()
    
            dataid.append(id)
            datacategory.append(category)
            
            dataline.append(line)
    #Get relative path to stanford models folder and select the file as a filename (stopwords);
    fileDirStop = os.path.dirname(os.path.realpath('__file__'))
    filenameStop = os.path.join(fileDirStop, 'conference_proceedings\\stop.txt')
    #Open the file, read out its contents (stopwords) and then close the file;
    file = open(filenameStop)
    stop = file.read()
    file.close()
    #Split the stopwords over \n.
    stop = stop.split('\n')
    #Filter the text based on the stopwords.
    dataline = [[w for w in line if not w in stop if len(w) > 1] for line in dataline]
    #Make training data and test data
    dataas = list(zip(dataid, datacategory, dataline))
    training_data = dataas[:19999]
    test_data =dataas[20000:20050]
    dataline = [[stemmer.stem(w) for w in line if not w in stop if w >1] for line in dataline]
    #Make training data and test data
    dataas = zip(dataid, datacategory, dataline)
    training_data = dataas[0:((n*m)-1)] +dataas[(((n+1)*m)+1):]
    test_data = dataas[(n*m):((n+1)*m)]
    return training_data, test_data

def sent2vec(sentence, vec):
    matrix = []
    for word in sentence:
        if word in vec:
            matrix.append(vec[word])
    vecrep = np.sum( np.array(matrix), axis=0 )
    return vecrep

def accuracy(test, outcome):
    counter = 0
    for i in range(len(test)):
        if (test[i] == outcome[i]):
            counter += 1
    uitslag = float(counter)/float((len(test)))
    return uitslag

def euclideanDistance(instance1, instance2, length):
    distance = 0
    distance = sum([ float(pow((instance1[x] - instance2[x]), 2)) for x in range(length)])
    return math.sqrt(distance)
    
#Open text and tokenize the data.
def main(n):
    print('Opening and preprocessing data...')
    accuracies = []
    Rick = []
    text = open_file('Data', pkl=False)
    text1 = [line.split('\t') for line in text.split('\n')]
    l = len(text1)
    m = int(math.floor(float(l)/float(n)))
    dat1 =[]
    for r in range(n):
        data, dat = process_data(text, r,m)

        sentences = [sentence for [id, cid, sentence]in data]
        categories = [cid for [id, cid, sentence]in data]
        ids= [id for [id, cid, sentence]in data]

        vec = gensim.models.Word2Vec(sentences, size=100, window=5, min_count=5, workers=4)


        model = [[ids[i], categories[i], sent2vec(sentences[i],vec)] for i in range(len(sentences))]


        testsentences = [sentence for [id, cid, sentence]in dat]
        testcids = [cid for [id, cid, sentence]in dat]
        categories = [cid for [id, cid, sentence]in data]
        outcomes = []
        count = 0

        for testsentence in testsentences:
            vecRep = sent2vec(testsentence,vec)
        ##    kneighbors = []
        ##    for [id, cid,sentence] in model:
        ##        dist = euclideanDistance(vecRep, sentence, len(sentence))          
        ##        #print(vecRep,sentence)
        ##        kneighbors.append([id, cid, dist])
        ##    kneighbors = sorted(kneighbors, key=itemgetter(2))
            t = time.time()
            kneighbors = [[cid, euclideanDistance(vecRep, sentence, len(sentence))] for [id, cid,sentence] in model]
            kneighbors = heapq.nsmallest(10,kneighbors,key=lambda kneighbors: kneighbors[1])
            t = time.time() -t
            print(t)
            cat = [cid for [cid, dist] in kneighbors]
            cat = [(10-i)*[cat[i]] for i in range(len(cat))]
            cat = [i for i in itertools.chain.from_iterable(cat)]

            mostCom = collections.Counter(cat).most_common(1)[0][0]
            
            outcomes.append(mostCom)
        accuracy1 = accuracy(testcids, outcomes)
        ik = [[testcids[i], outcomes[i]] for i in range(len(outcomes))]
        accuracies.append(accuracy1)
        Rick.append(ik)
    return accuracies, Rick

x, Rick = main(8)
text_file = open("Rick.txt", "w")
for sent in Rick:
    text_file.write(str(sent) + ".\n")
print(x)
    
