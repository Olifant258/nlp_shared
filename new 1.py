"""
Created on Tue Mar 15 14:12:14 2016

@author: Mark
"""
from __future__ import division
from operator import itemgetter
from sklearn.neighbors import NearestNeighbors
import os
import pickle
import math
#import xlwt
import collections
import random
import time
import logging

import gensim
import numpy as np

from stemming.porter2 import stem

#Function to open the data from the file and return it as the rough data.
def open_file(filePath, pkl=True):
    #Get relative path to conference proceedings folder and select the file as a filename;
    fileDir = os.path.dirname(os.path.realpath('__file__'))
    #Open the file, read out its contents and then close the file;
    if pkl == True:
        #Get path to file.
        filename = os.path.join(fileDir, filePath + '.pkl')
        #Open the file, pkl format.
        file = open(filename, 'rb')
        #Load the data.
        text = pickle.load(file)
        file.close()
    else:
        filename = os.path.join(fileDir, filePath + '.txt')
        file = open(filename)
        text = file.read()
        file.close()
    #and return the text data.
    return text
    
#Function to pre-process all data (tokenize into words). Returns training and test data.
def process_data(text):
    
    #Split up in matrix for each \n and \t in the document. Remain only with the titles of the papers.
    text = [line.split('\t') for line in text.split('\n')]
    #Format the data, so that we remain only with a list of lists of tokenized sentences.
    dataid = []
    datacategory = []
    dataline = []
    lineMov = '.,!?:;()-/\+<>' #Special characters to be removed.
    for line in text:
        if len(line) > 1:
            id = line[0]
            category = line[1]
            line = line[2:]
            line = line[0].lower() #Make all letters low cases.
            for i in range(len(lineMov)): #Remove special characters and replace them with a white space.
                line = line.replace(lineMov[i], ' ')
            #line = '<bos> ' + line + ' <eos>' #Add tokens for the start of the sentence and the end of the sentence.
            line = line.split()
    
            dataid.append(id)
            datacategory.append(category)
            
            dataline.append(line)
    from collections import defaultdict
    frequency = defaultdict(int)
    for text in dataline:
        for token in text:
            frequency[token] += 1
    
    dataline = [[token for token in text if frequency[token] > 1] for text in texts]
    #Get relative path to stanford models folder and select the file as a filename (stopwords);
    fileDirStop = os.path.dirname(os.path.realpath('__file__'))
    filenameStop = os.path.join(fileDirStop, 'conference_proceedings\\stop.txt')
    #Open the file, read out its contents (stopwords) and then close the file;
    file = open(filenameStop)
    stop = file.read()
    file.close()
    #Split the stopwords over \n.
    stop = stop.split('\n')
    #Filter the text based on the stopwords.
    dataline = [[w for w in line if not w in stop if w >1] for line in dataline]
    #Make training data and test data
    dataas = zip(dataid, datacategory, dataline)
    training_data = dataas[:19999]
    test_data =dataas[20000:20050]
    return training_data, test_data

def sent2vec(sentence):
    matrix = []
    for word in sentence:
        if word in vec:
            matrix.append(vec[word])
    vecrep = np.sum( np.array(matrix), axis=0 )
    return vecrep

def accuracy(test, outcome):
    counter = 0
    for i in range(len(test)):
        if (test[i] == outcome[i]):
            counter += 1
    uitslag = float(counter)/float((len(test)))
    return uitslag

def euclideanDistance(instance1, instance2, length):
    distance = 0
    for x in range(length):
##        print(instance1[x], instance2[x])
        distance += pow((instance1[x] - instance2[x]), 2)
    return math.sqrt(distance)
    
#Open text and tokenize the data.
print('Opening and preprocessing data...')
text = open_file('Data', pkl=False)
data, dat = process_data(text)
sentences = [sentence for [id, cid, sentence]in data]
categories = [cid for [id, cid, sentence]in data]
ids= [id for [id, cid, sentence]in data]

testsentences = [sentence for [id, cid, sentence]in dat]
##model = Word2Vec()

##model.build_vocab(sentences) #This strangely builds a vocab of "only" 747904 words which is << than those reported in the literature 10M words
##model.train(sentences)

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

words =[]
for sentence in sentences:
    for word in sentence:
        words.append(word)


##model.build_vocab(words) #This strangely builds a vocab of "only" 747904 words which is << than those reported in the literature 10M words
##model.train(words)

##words = [word for sentence in sentences for word in sentence if word is not '<bos>' if word is not '<eos>']
print(words[0:10])

testcategories = [cid for [id, cid, sentence]in dat]

vec = gensim.models.Word2Vec(words, size=10000, window=5, min_count=5, workers=4)
vec.train(sentences, total_words=None, word_count=0, total_examples=19999, queue_factor=2, report_delay=1.0)
newsentences = []

count1 = 0
count2 = 0

for sentence in sentences:
    words = []

    
    for word in sentence:
        count1 +=1
        if word in vec:
            words.append(vec[word].tolist())
        else:
            count2 +=1

    newsentences.append(words)

print(count1, count2)

##ns = list(newsentences)
##    
##newsentences = [[vec[word]] for sentence in sentences for word in sentence if word is not '<bos>' if word is not '<eos>']
print(len(newsentences))
newtestsentences = []

for sentence in testsentences:
    words = []
    
    
    for word in sentence:
        if word in vec:
            words.append(vec[word].tolist())

    newtestsentences.append(words)

##print(type(newtestsentences))
##nts = np.array(newtestsentences.tolist())




Output = [[categories[i],newsentences[i]] for i in range(len(ids))]
Output2 = [[testcategories[i],newtestsentences[i]] for i in range(len(testcategories))]
Final = Output + Output2
#print((Final[0]))

text_file = open("Final.txt", "w")
for [id, sent] in Final:
    text_file.write(id + str(sent) + ".\n")

pickle.dump( Final, open( "final.p", "wb" ) )
