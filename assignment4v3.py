__author__ = 'Rick Groenendijk'

import os
import xlwt
from assignment2 import freq_word, bigram_model, trigram_model, quadgram_model, log_probability, max_likelihood, uniprob

#Function to open the data from the file and return it as the rough data.
def open_file():
    #Get relative path to Wimbledon Tweets folder and select the file as a filename;
    fileDir = os.path.dirname(os.path.realpath('__file__'))
    filename = os.path.join(fileDir, 'conference_proceedings\\conference_proceedings.txt')
    #Open the file, read out its contents and then close the file;
    file = open(filename, encoding='latin')
    text = file.read()
    file.close()
    #and return the text data.
    return text

def filter_stopwords(line):

    return new_line

#Function to pre-process all data (tokenize into words). Returns training and test data.
def process_data(text):
    #Split up in matrix for each \n and \t in the document. Remain only with the titles of the papers.
    text = text.split('\n')
    temp = []
    for line in text:
        line = line.split('\t')
        temp.append(line[2])

    #Format the data, so that we remain only with a list of lists of tokenized sentences.
    data = []
    lineMov = '.,!?:;()-/+' #Special characters to be removed.
    for line in temp:
        line = line.lower() #Make all letters low cases.
        for i in range(len(lineMov)): #Remove special characters and replace them with a white space.
            line = line.replace(lineMov[i], ' ')
        line = '<bos> ' + line + ' <eos>' #Add tokens for the start of the sentence and the end of the sentence.
        line = line.split()
        data.append(line)

    #Get relative path to stanford models folder and select the file as a filename (stopwords);
    fileDirStop = os.path.dirname(os.path.realpath('__file__'))
    filenameStop = os.path.join(fileDirStop, 'stanford_models\\stop.txt')
    #Open the file, read out its contents (stopwords) and then close the file;
    file = open(filenameStop, encoding='latin')
    stop = file.read()
    file.close()
    #Split the stopwords over \n.
    stop = stop.split('\n')

    #Filter the text based on the stopwords.
    data = [[w for w in line if not w in stop] for line in data]

    #Make training data and test data
    training_data = data[:19999]
    test_data = data[20000:]
    return training_data, test_data




#CHANGE LATER.
def calculate(input):
    #Determine length of input.
    if len(input) == 1:
        output = uniprob(input, w, wf)
    elif len(input) == 2:
        w,wf = bigram_model(input)
        output = max_likelihood(w, wf, m2, mf2)*0.5
    elif len(input) == 3:
        m2,mf2 = trigram_model(input)
        output = max_likelihood(m2, mf2, m3, mf3, bigram=False)*0.67
    elif len(input) == 4:
        m3, mf3 = quadgram_model(input)
        output = max_likelihood(m3, mf3, m4, mf4, bigram=False)*0.75
    output = 1/(1+exp(-output))
    return input, output

def segment(A, e=5, u=4):
    U = []
    S1 = []
    S2 = []
    split_sen = A.split()
    l = len(split_sen)
    for i in range(l):
        S = []
        S.append(split_sen[:i])
        if i < u:
            #Do not split.
            input, score = calculate(S)
            U.append([input,score])
        for j in range (i-1):
            if (i-j <= u):
                S1 = S[0:j]
                S2 = S[j+1:]
                inputS2, score = calculate(S2)
            Sj = segment(S1,e=5,u=4)
            for segm in Sj:
                newS = segm + S2
                inputS, score = calculate(newS)
                Cs = inputS + inputS2
                U.append([newS,Cs])
        U.sort(key=lambda x: x[1])
    return U[-1][0]










#Open text and tokenize the data.
text = open_file()
training_data, test_data = process_data(text)

#Word frequencies and n-gram models
m2, mf2 = bigram_model(training_data)
m3, mf3 = trigram_model(training_data)
m4, mf4 = quadgram_model(training_data)
w, wf = freq_word(training_data)

tweet = 'My shoes are gg to compete in the youth olympic games sailing competition'

segment(tweet)
# tweet = tweet.lower()
# tweet = tweet.split()
# t = filter_stopwords(tweet)
#
# print(t)

print('YOU ARE TERMINATED!')