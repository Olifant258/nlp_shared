__author__ = 'Rick Groenendijk'

import os
import xlwt
from nltk.tag import StanfordNERTagger

#Defining the path to java (for nltk).
java_path = 'C:\\Program Files (x86)\\Java\\jdk1.8.0_73\\bin\\java.exe'
os.environ['JAVAHOME'] = java_path
#Defining class path to slf4j package (for nltk).
_CLASS_PATH = '.'
if os.environ.get('CLASSPATH') is not None:
    _CLASS_PATH = os.environ.get('CLASSPATH')
os.environ['CLASSPATH'] = _CLASS_PATH + ';C:\\Program Files (x86)\\Java\\slf4j-1.7.16\\slf4j-api-1.7.16.jar'
#Defining all paths to the stanford directory and the models therein.
stanford_ner_dir = 'C:\\Users\\Rick Groenendijk\\Documents\\ATLAS\\Semester 6\\Data Science\\Natural Language Processing\\stanford_models\\stanford-ner\\'
eng_model_filename_3class = stanford_ner_dir + 'classifiers\\english.all.3class.distsim.crf.ser.gz'
eng_model_filename_4class = stanford_ner_dir + 'classifiers\\english.conll.4class.distsim.crf.ser.gz'
eng_model_filename_7class = stanford_ner_dir + 'classifiers\\english.muc.7class.distsim.crf.ser.gz'
my_path_to_jar = stanford_ner_dir + 'stanford-ner.jar'

#Function to open the data from the file and return it as the rough data.
def open_file():
    #Get relative path to Wimbledon Tweets folder and select the file as a filename;
    fileDir = os.path.dirname(os.path.realpath('__file__'))
    filename = os.path.join(fileDir, 'NER_Twitter_Dataset\\NER_Twitter.txt')
    #Open the file, read out its contents and then close the file;
    file = open(filename, encoding='latin')
    text = file.read()
    file.close()
    #Split up the text over the cuts \n and remove the last empty element.
    text = text.split('\n')
    del text[-1]
    #Split up the text into entity-types and tweet text. Remove tweet ID.
    new_text = []
    for line in text:
        line = line.split('\t')
        line = line [1:]
        new_text.append(line)
    #Remove some special characters.
    text = []
    lineMov = '.,!?:;()='
    for line in new_text:
        for i in range(len(lineMov)):
            line[1] = line[1].replace(lineMov[i], '')
        text.append(line)
    #Return the data.
    return text

#Function to reformat the data so that it matches the format as used by the Stanford NER tagger.
def class_split(line):
    #Split the line along ;.
    line = line.split(';')
    del line[-1]
    #Loop over the line and
    classes = []
    for set in line:
        if set[:3] == 'PER':
            set = set.replace(set[:4], '')
            #set = set.split()
            # for word in set:
            #     classes.append((word, 'PERSON'))
            classes.append((set, 'PERSON'))
        elif set[:3] == 'ORG':
            set = set.replace(set[:4], '')
            #set = set.split()
            # for word in set:
            #     classes.append((word, 'ORGANIZATION'))
            classes.append((set, 'ORGANIZATION'))
        elif set[:3] == 'LOC':
            set = set.replace(set[:4], '')
            #set = set.split()
            # for word in set:
            #     classes.append((word, 'LOCATION'))
            classes.append((set, 'LOCATION'))
        elif set[:4] == 'MISC':
            set = set.replace(set[:5], '')
            #set = set.split()
            # for word in set:
            #     classes.append((word, 'MISC'))
            classes.append((set, 'MISC'))
    return classes

#Function to return the position in the confusion matrix.
def conf_pos(tag):
    if tag == 'PERSON':
        pos = 0
    elif tag == 'LOCATION':
        pos = 1
    elif tag == 'ORGANIZATION':
        pos = 2
    elif tag == 'MISC':
        pos = 3
    else:
        pos = 4
    return pos

#Function to reformat a line completely into the optimal output of the StanfordNER tagger.
def token(line):
    line[1] = line[1].split()
    classes = []
    #Make a list of splitted classes and their classification.
    for part in line[0]:
        if ' ' in part[0]:
            new_parts = part[0].split()
            for p in new_parts:
                classes.append((p, part[1]))
        else:
            classes.append(part)
    #Put these splitted classes back in the sentence.
    for part in classes:
        if part[0] in line[1]:
            line[1][line[1].index(part[0])] = part
    #Make the unclassified elements tuples of (word, 'O').
    for elem in line[1]:
        if isinstance(elem, str):
            line[1][line[1].index(elem)] = (elem, 'O')
    return line


#Function to let the Stanford tagger run over the data and return compare that to the classified data.
def tagger(data, class_amount=4):
    #Choose the model, corresponding to the class_amount.
    if class_amount == 3:
        model_name = eng_model_filename_3class
    elif class_amount == 4:
        model_name = eng_model_filename_4class
    elif class_amount == 7:
        model_name = eng_model_filename_7class
    #Build model.
    st = StanfordNERTagger(model_filename=model_name, path_to_jar=my_path_to_jar)

    #Define all tables for storage of classification.
    conf_matrix = [[0]*5 for i in range(5)]

    #Go through data and use the chosen model for classification.
    counter = 0
    for line in data:
        #To show how far we are.
        if counter % 100 == 0:
            print('class_amount =', class_amount, 'count', counter)
        counter += 1
        #Tag the words in the sentence #(and remove all 'O' from the list.)
        tag = st.tag(line[1].split())
        #Tokenize the line:
        tok = token(line)
        #Loop over all tags to see if the sentence's ideal tags match up.
        pos = 0
        i = 0
        while i < len(tag):
            #In case the tags on both the prediction set and the actual set match.
            if tag[i] == tok[1][i]:
                #Add one to the confusion matrix at the right indices.
                conf_matrix[conf_pos(tag[i][1])][conf_pos(tag[i][1])] += 1
                #If the tag also appears in a complete set of tags in this segment.
                if len(tok[0]) > 0 and tag[i][0] in tok[0][pos][0].split():
                    #Increase i to skip over the rest of the segment.
                    i += (len(tok[0][pos][0].split()) - 1)
                    #If there is multiple classes, increase p to find the next segment in the next iteration.
                    if pos < len(tok[0]) - 1 and len(tok[0]) > 1:
                        pos += 1
            else:
                conf_matrix[conf_pos(tag[i][1])][conf_pos(tok[1][i][1])] += 1
                if len(tok[0]) > 0 and tag[i][0] in tok[0][pos][0].split():
                    i += (len(tok[0][pos][0].split()) - 1)
                    if pos < len(tok[0]) - 1 and len(tok[0]) > 1:
                        pos += 1
            i += 1
    return conf_matrix

#Function that returns micro-averaged precision, recall and F1-measure.
def performance(m):
    #Sum the total amount of elements in the 4x4 matrix.
    s = sum([sum(line) for line in m])
    #Create 4 2x2 confusion matrices.
    per_m = [m[0][0], sum(m[0]) - m[0][0], sum([r[0] for r in m]) - m[0][0], 0]
    per_m[3] = s - sum(per_m)
    loc_m = [m[1][1], sum(m[1]) - m[1][1], sum([r[1] for r in m]) - m[1][1], 0]
    loc_m[3] = s - sum(loc_m)
    org_m = [m[2][2],  sum(m[2]) - m[2][2], sum([r[2] for r in m]) - m[2][2], 0]
    org_m[3] = s - sum(org_m)
    misc_m = [m[3][3], sum(m[3]) - m[3][3], sum([r[3] for r in m]) - m[3][3], 0]
    misc_m[3] = s - sum(misc_m)
    #Put together combined confusion matrix.
    a = per_m[0] + loc_m[0] + org_m[0] + misc_m[0]
    b = per_m[1] + loc_m[1] + org_m[1] + misc_m[1]
    c = per_m[2] + loc_m[2] + org_m[2] + misc_m[2]
    #Now calculate micro-averaged precision, recall and F1-measure.
    p = a/(a+b)
    r = a/(a+c)
    f = (2*p*r)/(p+r)
    return p, r, f

#Open the Twitter NER set.
data = open_file()
#Loop of the data and set the format for class indicators to equal that of Stanford NERtagger.
data = [[class_split(line[0]), line[1]] for line in data]

p3, r3, f3 = performance(tagger(data, class_amount=3))
data = open_file()
data = [[class_split(line[0]), line[1]] for line in data]
p7, r7, f7 = performance(tagger(data, class_amount=7))
data = open_file()
data = [[class_split(line[0]), line[1]] for line in data]
m4 = tagger(data)
p4, r4, f4 = performance(m4)
#Print all results, in case saving to excel file fails.
print('3-class', p3, r3, f3)
print('4-class', p4, r4, f4)
print('7-class', p7, r7, f7)
print('Confusion matrix 4-class')
for i in range(5):
    print(m4[i])

#Create a book and 2 sheets to save the data to.
book = xlwt.Workbook(encoding='latin')
sheet1 = book.add_sheet("Performance NER taggers")
#Write some titles and such to the sheets.
sheet1.write(0,0,'Performance of classifiers')
sheet1.write(1,1,'3-class')
sheet1.write(1,2,'4-class')
sheet1.write(1,3,'7-class')
sheet1.write(2,0,'Precision')
sheet1.write(3,0,'Recall')
sheet1.write(4,0,'F1-measure')
#Write data to sheet.
sheet1.write(2,1,p3)
sheet1.write(2,2,p4)
sheet1.write(2,3,p7)
sheet1.write(3,1,r3)
sheet1.write(3,2,r4)
sheet1.write(3,3,r7)
sheet1.write(4,1,f3)
sheet1.write(4,2,f4)
sheet1.write(4,3,f7)
#Write confusion matrix titles to sheet.
sheet1.write(6,0,'Confusion Matrix 4-class')
classes = ['PERSON', 'LOCATION', 'ORGANIZATION', 'MISC', 'NONE']
for i in range(len(classes)):
    sheet1.write(8+i,0,classes[i])
    sheet1.write(7, i+1, classes[i])
#Write data to the sheet.
for i in range(5):
    for j in range(5):
        sheet1.write(8+i, 1+j, m4[i][j])
#Save sheet to file, which is the deliverable for this assignment.
book.save('assignment3_Romme&Groenendijk.xls')
print('YOU ARE TERMINATED!')