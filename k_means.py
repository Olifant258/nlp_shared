"""
Created on Tue Mar 15 14:12:14 2016

@author: Mark
"""
from __future__ import division
from operator import itemgetter
from sklearn.neighbors import NearestNeighbors
import os
import pickle
import math
#import xlwt
import collections
import random
import time
import gensim, logging
import numpy as np
import word2vec
##from word2vec import Word2Vec, Sent2Vec, LineSentence

from gensim import utils
from gensim.models.doc2vec import LabeledSentence
from gensim.models import Doc2Vec


from stemming.porter2 import stem

#Function to open the data from the file and return it as the rough data.
def open_file(filePath, pkl=True):
    #Get relative path to conference proceedings folder and select the file as a filename;
    fileDir = os.path.dirname(os.path.realpath('__file__'))
    #Open the file, read out its contents and then close the file;
    if pkl == True:
        #Get path to file.
        filename = os.path.join(fileDir, filePath + '.pkl')
        #Open the file, pkl format.
        file = open(filename, 'rb')
        #Load the data.
        text = pickle.load(file)
        file.close()
    else:
        filename = os.path.join(fileDir, filePath + '.txt')
        file = open(filename)
        text = file.read()
        file.close()
    #and return the text data.
    return text
    
#Function to pre-process all data (tokenize into words). Returns training and test data.
def process_data(text):
    
    #Split up in matrix for each \n and \t in the document. Remain only with the titles of the papers.
    text = [line.split('\t') for line in text.split('\n')]
    #Format the data, so that we remain only with a list of lists of tokenized sentences.
    dataid = []
    datacategory = []
    dataline = []
    lineMov = '.,!?:;()-/\+<>' #Special characters to be removed.
    for line in text:
        if len(line) > 1:
            id = line[0]
            category = line[1]
            line = line[2:]
            line = line[0].lower() #Make all letters low cases.
            for i in range(len(lineMov)): #Remove special characters and replace them with a white space.
                line = line.replace(lineMov[i], ' ')
##            line = '<bos> ' + line + ' <eos>' #Add tokens for the start of the sentence and the end of the sentence.
            line = line.split()
    
            dataid.append(id)
            datacategory.append(category)
            
            dataline.append(line)
    #Get relative path to stanford models folder and select the file as a filename (stopwords);
    fileDirStop = os.path.dirname(os.path.realpath('__file__'))
    filenameStop = os.path.join(fileDirStop, 'conference_proceedings\\stop.txt')
    #Open the file, read out its contents (stopwords) and then close the file;
    file = open(filenameStop)
    stop = file.read()
    file.close()
    #Split the stopwords over \n.
    stop = stop.split('\n')
    #Filter the text based on the stopwords.
    dataline = [[w for w in line if not w in stop if w >1] for line in dataline]
    dataline = [[" ".join(line)] for line in dataline]
    print(dataline[0])
    #Make training data and test data
    dataas = zip(dataid, datacategory, dataline)
    training_data = dataas[:19999]
    test_data =dataas[20000:20050]
    return training_data, test_data

def sent2vec(sentence):
    matrix = []
    for word in sentence:
        if word in vec:
            matrix.append(vec[word])
    vecrep = np.sum( np.array(matrix), axis=0 ).tolist()
    return vecrep

def accuracy(test, outcome):
    counter = 0
    for i in range(len(test)):
        if (test[i] == outcome[i]):
            counter += 1
    uitslag = float(counter)/float((len(test)))
    return uitslag

def euclideanDistance(instance1, instance2, length):
    distance = 0
    for x in range(length):
##        print(instance1[x], instance2[x])
        distance += pow((instance1[x] - instance2[x]), 2)
    return math.sqrt(distance)
    
#Open text and tokenize the data.
print('Opening and preprocessing data...')
text = open_file('Data', pkl=False)
data, dat = process_data(text)
sentences = [sentence for [id, cid, sentence]in data]

categories = [cid for [id, cid, sentence]in data]
ids= [id for [id, cid, sentence]in data]

vec = gensim.models.Word2Vec(sentences, size=1000, window=5, min_count=5, workers=4)
##vec = word2vec.Sent2Vec(sentences, size=100, window=8, min_count=5, workers=4)
##
model = [[ids[i], categories[i], sent2vec(sentences[i])] for i in range(len(sentences))]

##
testsentences = [sentence for [id, cid, sentence]in dat]
testcids = [cid for [id, cid, sentence]in dat]
categories = [cid for [id, cid, sentence]in data]
outcomes = []
count = 0

k_means = 5*[0]
count1 = 0
count2 = 0
count3 = 0
count4 = 0
count5 = 0

for [id, cid, sentence] in data:
    if cid == 'ISCAS':
        count1+=1
        if k_means[0] > 1:
            k_means[0] += [x + y for x, y in zip(sentence, k_means[0])]
        else:
            k_means[0] = sentence
        
    if cid == 'VLDB':
        count2+=1
        if k_means[1] > 1:
            k_means[1] += [x + y for x, y in zip(sentence, k_means[1])]
        else:
            k_means[1] = sentence
    if cid == 'INFOCOM':
        count3+=1
        if k_means[2] > 1:
            k_means[2] += [x + y for x, y in zip(sentence, k_means[2])]
        else:
            k_means[2] = sentence
    if cid == 'SIGGRAPH':
        count4+=1
        if k_means[3] > 1:
            k_means[3] += [x + y for x, y in zip(sentence, k_means[3])]
        else:
            k_means[3] = sentence
    if cid == 'WWW':
        count5+=1
        if k_means[4] > 1:
            k_means[4] += [x + y for x, y in zip(sentence, k_means[4])]
        else:
            k_means[4] = sentence

k_means[0] = k_means[0]/count1
k_means[1] = k_means[1]/count2
k_means[2] = k_means[2]/count3
k_means[3] = k_means[3]/count4
k_means[4] = k_means[4]/count5


for testsentence in testsentences:
    vecRep = sent2vec(testsentence)
    distance = []
    for i in range(len(k_means)):
        dist = euclideanDistance(vecRep, k_means[i], len(vecRep))          
        distance.append([i, dist])
    distance = sorted(distance, key=itemgetter(1))
    t = time.time()
    print(distance)
    cat = [id for [id, dist] in distance]
    mostCom = cat[0]
    
    outcomes.append(mostCom)

for i in range(len(outcomes)):
    if outcomes[i] ==0:
        outcomes[i] = 'ISCAS'
    if outcomes[i] ==1:
        outcomes[i] = 'VLDB'
    if outcomes[i] ==2:
        outcomes[i] = 'INFOCOM'
    if outcomes[i] ==3:
        outcomes[i] = 'SIGGRAPH'
    if outcomes[i] ==4:
        outcomes[i] = 'WWW'

print(testcids[0:10])
print(outcomes[0:10])
accuracy = accuracy(testcids, outcomes)

print(accuracy)


    
