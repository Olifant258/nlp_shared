# -*- coding: utf-8 -*-
"""
Created on Tue Mar 15 14:12:14 2016

@author: Mark
"""
from __future__ import division
import os
import pickle
import math
#import xlwt
import random
import time



from stemming.porter2 import stem

#Function to open the data from the file and return it as the rough data.
def open_file(filePath, pkl=True):
    #Get relative path to conference proceedings folder and select the file as a filename;
    fileDir = os.path.dirname(os.path.realpath('__file__'))
    #Open the file, read out its contents and then close the file;
    if pkl == True:
        #Get path to file.
        filename = os.path.join(fileDir, filePath + '.pkl')
        #Open the file, pkl format.
        file = open(filename, 'rb')
        #Load the data.
        text = pickle.load(file)
        file.close()
    else:
        filename = os.path.join(fileDir, filePath + '.txt')
        file = open(filename)
        text = file.read()
        file.close()
    #and return the text data.
    return text
    
#Function to pre-process all data (tokenize into words). Returns training and test data.
def process_data(text):
    
    #Split up in matrix for each \n and \t in the document. Remain only with the titles of the papers.
    text = [line.split('\t') for line in text.split('\n')]
    #Format the data, so that we remain only with a list of lists of tokenized sentences.
    dataid = []
    datacategory = []
    dataline = []
    lineMov = '.,!?:;()-/\+<>' #Special characters to be removed.
    for line in text:
        if len(line) > 1:
            id = line[0]
            category = line[1]
            line = line[2:]
            line = line[0].lower() #Make all letters low cases.
            for i in range(len(lineMov)): #Remove special characters and replace them with a white space.
                line = line.replace(lineMov[i], ' ')
            line = ['<bos>'] + [word for word in line.split() if len(word) > 1] + ['<eos>'] #Add tokens for the start of the sentence and the end of the sentence.

            dataid.append(id)
            datacategory.append(category)

            dataline.append(line)
    #Get relative path to stanford models folder and select the file as a filename (stopwords);
    fileDirStop = os.path.dirname(os.path.realpath('__file__'))
    filenameStop = os.path.join(fileDirStop, 'conference_proceedings\\stop.txt')
    #Open the file, read out its contents (stopwords) and then close the file;
    file = open(filenameStop)
    stop = file.read()
    file.close()
    #Split the stopwords over \n.
    stop = stop.split('\n')
    #Filter the text based on the stopwords.
    dataline = [[w for w in line if not w in stop if w >1] for line in dataline]
    #Make training data and test data
    dataas = list(zip(dataid, datacategory, dataline))
    training_data = dataas[:19999]
    test_data = dataas[20000:]
    return training_data, test_data
    
    
#Open text and tokenize the data.
print('Opening and preprocessing data...')
text = open_file('Data', pkl=False)
training_data, test_data = process_data(text)

time1 = time.time()

wordcountSIGGRAPH={}
wordcountISCAS={}
wordcountVLDB={}
wordcountINFCOM={}
wordcountWWW={}

for line in training_data:
    line1 = line[2]
    if line[1] == 'ISCAS':
        line2 = line[2][1:]
        for word in line1:
            if word not in wordcountISCAS:
                wordcountISCAS[word] = 2
            else:
                wordcountISCAS[word] += 1
    if line[1] == 'VLDB':
        for word in line1:
            if word not in wordcountVLDB:
                wordcountVLDB[word] = 2
            else:
                wordcountVLDB[word] += 1
    if line[1] == 'INFOCOM':
        for word in line1:
            if word not in wordcountINFCOM:
                wordcountINFCOM[word] = 2
            else:
                wordcountINFCOM[word] += 1
    if line[1] == 'SIGGRAPH':
        for word in line1:
            if word not in wordcountSIGGRAPH:
                wordcountSIGGRAPH[word] = 2
            else:
                wordcountSIGGRAPH[word] += 1
    if line[1] == 'WWW':
        for word in line1:
            if word not in wordcountWWW:
                wordcountWWW[word] = 2
            else:
                wordcountWWW[word] += 1

<<<<<<< HEAD

word2countSIGGRAPH={}
word2countISCAS={}
word2countVLDB={}
word2countINFCOM={}
word2countWWW={}

for line in training_data:
    line5 = line[2]
    line2 = line5[1:]
    bigrams = [line5[i] + ' ' + line2[i] for i in range(len(line2))]
    if line[1] == 'ISCAS':
        for word in bigrams:
            if word not in word2countISCAS:
                word2countISCAS[word] = 2
            else:
                word2countISCAS[word] += 1
    if line[1] == 'VLDB':
        for word in bigrams:
            if word not in word2countVLDB:
                word2countVLDB[word] = 2
            else:
                word2countVLDB[word] += 1
    if line[1] == 'INFOCOM':
        for word in bigrams:
            if word not in word2countINFCOM:
                word2countINFCOM[word] = 2
            else:
                word2countINFCOM[word] += 1
    if line[1] == 'SIGGRAPH':
        for word in bigrams:
            if word not in word2countSIGGRAPH:
                word2countSIGGRAPH[word] = 2
            else:
                word2countSIGGRAPH[word] += 1
    if line[1] == 'WWW':
        for word in bigrams:
            if word not in word2countWWW:
                word2countWWW[word] = 2
            else:
                word2countWWW[word] += 1

=======
# print(sum(wordcountWWW.itervalues()))
# word2countSIGGRAPH={}
# word2countISCAS={}
# word2countVLDB={}
# word2countINFCOM={}
# word2countWWW={}
#
# for line in training_data:
#     line1 = line[2]
#     line2 = line1[1:]
#     bigrams = [line1[i] + ' ' + line2[i] for i in range(len(line2))]
#     if line[1] == 'ISCAS':
#         for word in bigrams:
#             if word not in word2countISCAS:
#                 word2countISCAS[word] = 2
#             else:
#                 word2countISCAS[word] += 1
#     if line[1] == 'VLDB':
#         for word in bigrams:
#             if word not in word2countVLDB:
#                 word2countVLDB[word] = 2
#             else:
#                 word2countVLDB[word] += 1
#     if line[1] == 'INFOCOM':
#         for word in bigrams:
#             if word not in word2countINFCOM:
#                 word2countINFCOM[word] = 2
#             else:
#                 word2countINFCOM[word] += 1
#     if line[1] == 'SIGGRAPH':
#         for word in bigrams:
#             if word not in word2countSIGGRAPH:
#                 word2countSIGGRAPH[word] = 2
#             else:
#                 word2countSIGGRAPH[word] += 1
#     if line[1] == 'WWW':
#         for word in bigrams:
#             if word not in word2countWWW:
#                 word2countWWW[word] = 2
#             else:
#                 word2countWWW[word] += 1
#
>>>>>>> c35ee2fe573b745d1228ee85276d1377163b1b78
##print(wordcountISCAS[0:10])
totISCAS = sum(wordcountISCAS.values())
totVLDB = sum(wordcountVLDB.values())
totINFCOM = sum(wordcountINFCOM.values())
totSIGGRAPH = sum(wordcountSIGGRAPH.values())
totWWW = sum(wordcountWWW.values())
#
# tot2ISCAS = sum(word2countISCAS.values())
# tot2VLDB = sum(word2countVLDB.values())
# tot2INFCOM = sum(word2countINFCOM.values())
# tot2SIGGRAPH = sum(word2countSIGGRAPH.values())
# tot2WWW = sum(word2countWWW.values())

print(totINFCOM)

probs = [0,0,0,0,0]
probs2 = []
kansen = []
kansen2=[]

for [id, cid, line] in test_data:
    #line = line[1:-1]
    probs = [0,0,0,0,0]
    for word in line:
        if word in wordcountISCAS:
            probs[0] += (math.log(float(float(wordcountISCAS[word])/float(totISCAS))))
        else:
            probs[0] += (math.log(float(float(1)/float(totISCAS))))
        if word in wordcountVLDB:
            probs[1] += (math.log(float(float(wordcountVLDB[word])/float(totVLDB))))
        else:
            probs[1] += (math.log(float(float(1)/float(totVLDB))))
        if word in wordcountINFCOM:
            probs[2] += math.log(float(float(wordcountINFCOM[word])/float(totINFCOM)))
        else:
            probs[2] += math.log(float(float(1)/float(totINFCOM)))
        if word in wordcountSIGGRAPH:
            probs[3] += math.log(float(float(wordcountSIGGRAPH[word])/float(totSIGGRAPH)))
        else:
            probs[3] += math.log(float(float(1)/float(totSIGGRAPH)))
        if word in wordcountWWW:
            probs[4] += math.log(float(float(wordcountWWW[word])/float(totWWW)))
        else:
            probs[4] += math.log(float(float(1)/float(totWWW)))
    kansen.append([probs[0], probs[1], probs[2], probs[3], probs[4]])

<<<<<<< HEAD
for [id, cid, line] in test_data:
    line = line[1:-1]
    line2 = line[1:]
    line3 = [line[i] + ' ' + line2[i] for i in range(len(line2))]
    line = line3
    probs2 = [0,0,0,0,0]
    for word in line:
        if word in word2countISCAS:
            probs2[0] += (math.log(float(float(word2countISCAS[word])/float(totISCAS))))
        else:
            probs2[0] += (math.log(float(float(1)/float(totISCAS))))
        if word in word2countVLDB:
            probs2[1] += (math.log(float(float(word2countVLDB[word])/float(totVLDB))))
        else:
            probs2[1] += (math.log(float(float(1)/float(totVLDB))))
        if word in word2countINFCOM:
            probs2[2] += math.log(float(float(word2countINFCOM[word])/float(totINFCOM)))
        else:
            probs2[2] += math.log(float(float(1)/float(totINFCOM)))
        if word in word2countSIGGRAPH:
            probs2[3] += math.log(float(float(word2countSIGGRAPH[word])/float(totSIGGRAPH)))
        else:
            probs2[3] += math.log(float(float(1)/float(totSIGGRAPH)))
        if word in word2countWWW:
            probs2[4] += math.log(float(float(word2countWWW[word])/float(totWWW)))
        else:
            probs2[4] += math.log(float(float(1)/float(totWWW)))
        #print(probs2)
    kansen2.append([probs2[0], probs2[1], probs2[2], probs2[3], probs2[4]])
=======
# for [id, cid, line] in test_data:
#     #line1 = line[1:-1]
#     line2 = line[1:]
#     line3 = [line[i] + ' ' + line2[i] for i in range(len(line2))]
#     line = line3
#     probs2 = [0,0,0,0,0]
#     for word in line:
#         if word in word2countISCAS:
#             probs2[0] += (math.log(float(float(word2countISCAS[word])/float(totISCAS))))
#         else:
#             probs2[0] += (math.log(float(float(1)/float(totISCAS))))
#         if word in word2countVLDB:
#             probs2[1] += (math.log(float(float(word2countVLDB[word])/float(totVLDB))))
#         else:
#             probs2[1] += (math.log(float(float(1)/float(totVLDB))))
#         if word in word2countINFCOM:
#             probs2[2] += math.log(float(float(word2countINFCOM[word])/float(totINFCOM)))
#         else:
#             probs2[2] += math.log(float(float(1)/float(totINFCOM)))
#         if word in word2countSIGGRAPH:
#             probs2[3] += math.log(float(float(word2countSIGGRAPH[word])/float(totSIGGRAPH)))
#         else:
#             probs2[3] += math.log(float(float(1)/float(totSIGGRAPH)))
#         if word in word2countWWW:
#             probs2[4] += math.log(float(float(word2countWWW[word])/float(totWWW)))
#         else:
#             probs2[4] += math.log(float(float(1)/float(totWWW)))
#         #print(probs2)
#     kansen2.append([probs2[0], probs2[1], probs2[2], probs2[3], probs2[4]])
>>>>>>> c35ee2fe573b745d1228ee85276d1377163b1b78


Outcome = []
Outcome2 = []

# kans = []
# for i in range(len(kansen)):
#     subl = [0.5*(kansen[i][o] +kansen2[i][o]) for o in range(5)]
#     kans.append(subl)

for a in range(len(kansen)):
    maxi = max(kansen[a])
    
    winner = kansen[a].index(maxi)
    Outcome.append(winner)

<<<<<<< HEAD
for a in range(len(kansen2)):
    maxi = max(kansen2[a])
    winner = kansen2[a].index(maxi)
    Outcome2.append(winner)
=======
# for a in range(len(kans)):
#     maxi = max(kans[a])
#     winner = kans[a].index(maxi)
#     Outcome2.append(winner)
>>>>>>> c35ee2fe573b745d1228ee85276d1377163b1b78

        

Uistlag = []
for x in Outcome:
    if x == 0:
        Uistlag.append('ISCAS')
    if x == 1:
        Uistlag.append('VLDB')
    if x == 2:
        Uistlag.append('INFOCOM')
    if x == 3:
        Uistlag.append('SIGGRAPH')
    if x == 4:
        Uistlag.append('WWW')
        


categories =[cid for [id,cid,line] in test_data]

<<<<<<< HEAD
Uitslag = []
lijst = [random.randrange(0,4) for _ in range(len(categories))]
for x in Outcome2:
    if x == 0:
        Uitslag.append('ISCAS')
    if x == 1:
        Uitslag.append('VLDB')
    if x == 2:
        Uitslag.append('INFOCOM')
    if x == 3:
        Uitslag.append('SIGGRAPH')
    if x == 4:
        Uitslag.append('WWW')

input = ['ISCAS', 'VLDB', 'INFOCOM', 'SIGGRAPH', 'WWW']
=======
# Uitslag = []
# lijst = [random.randrange(0,4) for _ in range(len(categories))]
# for x in Outcome2:
#     if x == 0:
#         Uitslag.append('ISCAS')
#     if x == 1:
#         Uitslag.append('VLDB')
#     if x == 2:
#         Uitslag.append('INFCOM')
#     if x == 3:
#         Uitslag.append('SIGGRAPH')
#     if x == 4:
#         Uitslag.append('WWW')
#
# input = ['ISCAS', 'VLDB', 'INFCOM', 'SIGGRAPH', 'WWW']
>>>>>>> c35ee2fe573b745d1228ee85276d1377163b1b78

print('Done')

count = 0
total = 0
for i in range(len(categories)):
    if categories[i] == Uistlag[i]:
        count +=1
    total +=1

    

print('Count is', count)
print('Total is', total)
print('Accuracy is', float(count)/float(total))

# count = 0
# total = 0
# for i in range(len(categories)):
#     if categories[i] == Uitslag[i]:
#         count +=1
#     total +=1
#
#
#
# print('Count is', count)
# print('Total is', total)
# print('Accuracy is', float(count)/float(total))

time1 = time.time()-time1
print(time1)
#[[voorsp, cat] for voorsp in Outcome for cat in categories]

    
               
##
##
##
##
##
##
##
##
##
##
##
##
##tekst = [line[2] for line in training_data]
##uitkomst = [x  for y in tekst for x in y if x !=('<bos>' or '<eos>')]
##
##tot = len(uitkomst)

##
##
##for line in test data
##
##
