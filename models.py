__author__ = 'Rick Groenendijk'

import os
import pickle
from collections import Counter

#Function to open the data from the file and return it as the rough data.
def open_file(filePath, fileType):
    #Get relative path to the conference proceedings folder and select the file as a filename;
    fileDir = os.path.dirname(os.path.realpath('__file__'))
    filename = os.path.join(fileDir, 'conference_proceedings\\' + filePath + fileType)
    #Open the file, read out its contents and then close the file;
    file = open(filename, encoding='latin')
    text = file.read()
    file.close()
    #and return the text data.
    return text

#Function to save the data to a file.
def save_to_file(filename, data):
    #Get relative path to the conference proceedings folder and select the file as a filename;
    fileDir = os.path.dirname(os.path.realpath('__file__'))
    filePath = os.path.join(fileDir, 'conference_proceedings\\' + filename + '.pkl')
    #Write data to file in pkl format.
    output = open(filePath, 'wb')
    pickle.dump(data, output, -1)
    output.close()
    #return empty 1.
    return 1

#Function to pre-process all data (tokenize into words and remove stopwords). Returns training and test data.
def process_data(text):
    #Split up in matrix for each \n and \t in the document. Remain only with the titles of the papers.
    text = [line.split('\t')[2] for line in text.split('\n')]
    #Format the data, so that we remain only with a list of lists of tokenized sentences.
    data = []
    lineMov = '.,!?:;()-/\+<>' #Special characters to be removed.
    for line in text:
        line = line.lower() #Make all letters low cases.
        for i in range(len(lineMov)): #Remove special characters and replace them with a white space.
            line = line.replace(lineMov[i], ' ')
        #Add tokens for the start of the sentence and the end of the sentence and split along tokens.
        line = ['<bos>'] + [word for word in line.split() if len(word) > 1] + ['<eos>']
        data.append(line)
    #Open the file, read out its contents (stopwords) and then close the file; Then split over '\n'.
    stop = open_file('stop', '.txt').split('\n')
    #Filter the text based on the stopwords.
    data = [[w for w in line if not w in stop] for line in data]
    #Make training data and test data
    training_data = data[:19999]
    test_data = data[20000:]
    return training_data, test_data

#Function to make a model for unigrams, returning tokens and their counts.
def unigram_model(data):
    #Flatten the list of sub-lists.
    words = [item for sublist in data for item in sublist]
    #Make a dictionary for all words and their frequencies.
    dic = [[key,value+1] for key,value in Counter(words).items()]
    #Now split these up in seperate list to ease further lookups. Also add '<unknown>' token with 1 occurrence.
    model = [[word[0]] for word in dic]
    freq = [word[1] for word in dic]
    #return the model and frequencies.
    return [model, freq]

#Function to make a list of all bigram in the data. Returns the bigrams and their frequencies.
def bigram_model(data): #, unigram):
    #Flatten the list of sub-lists and find all bigrams.
    word1 = [item for sublist in data for item in sublist]
    word2 = word1[1:]
    data = [list(i) for i in zip(word1, word2) if '<eos>' not in i[0]]
    #Make a dictionary for all bigrams and their frequencies.
    dic = [(a, b, v) for (a, b), v in Counter(map(tuple, data)).items()]
    #Now split these up in separate list to ease further lookups.
    model = [[word[0], word[1]] for word in dic]
    freq = [word[2] for word in dic]
    #return the model and frequencies.
    return [model, freq]

#Function to make a list of all trigram in the data. Returns the trigrams and their frequencies.
def trigram_model(data):
    #Flatten the list of sub-lists and find all trigrams.
    word1 = [item for sublist in data for item in sublist]
    word2 = word1[1:]
    word3 = word1[2:]
    data = [list(i) for i in zip(word1, word2, word3) if '<eos>' not in i[:2]]
    #Make a dictionary for all trigrams and their frequencies.
    dic = [(a, b, c, v) for (a, b, c), v in Counter(map(tuple, data)).items()]
    #Now split these up in separate list to ease further lookups.
    model = [[word[0], word[1], word[2]] for word in dic]
    freq = [word[3] for word in dic]
    #return the model and frequencies.
    return [model, freq]

#Function to make a list of all quadgram in the data. Returns the quadgrams and their frequencies.
def quadgram_model(data):
    #Flatten the list of sub-lists and find all quadgrams.
    word1 = [item for sublist in data for item in sublist]
    word2 = word1[1:]
    word3 = word1[2:]
    word4 = word1[3:]
    data = [list(i) for i in zip(word1, word2, word3, word4)  if '<eos>' not in i[:3]]
    #Make a dictionary for all quadgrams and their frequencies.
    dic = [(a, b, c, d, v) for (a, b, c, d), v in Counter(map(tuple, data)).items()]
    #Now split these up in separate list to ease further lookups.
    model = [[word[0], word[1], word[2], word[3]] for word in dic]
    freq = [word[4] for word in dic]
    #return the model and frequencies.
    return [model, freq]

#----OUTDATED------------------------------
# def bigram_model(data, unigram):
#     #SEE FUNCTION ON TOP.
#
#     V = len(freq) #V = len(unigram[1])
#     #Now make a model with actual probabilities in stead of counts, using Laplace smoothing.
#     bi_model = [(freq[i] + 1) / (V + unigram[1][unigram[0].index(model[i][0])]) for i in range(len(freq))]
#     return [model, freq, bi_model]
#
# #Function to make a list of all trigram in the data. Returns the trigrams and their frequencies.
# def trigram_model(data, bigram, V):
#     #SEE FUNCTION ON TOP.
#
#     #Return all possible bigrams, which is the square of the vocabulary.
#     V = len(freq) #V = V ** 2
#     #Now make a model with actual probabilities in stead of occurrences.
#     tri_model = [(freq[i] + 1) / (V + bigram[1][bigram[0].index(model[i][:2])]) for i in range(len(freq))]
#     return [model, freq, tri_model]
#
# #Function to make a list of all quadgram in the data. Returns the quadgrams and their frequencies.
# def quadrigram_model(data, trigram, V):
#     #SEE FUNCTION ON TOP.
#
#     #Return all possible trigrams, which is the cube of the vocabulary.
#     V = len(freq) #V = v ** 3
#     #Now make a model with actual probabilities in stead of occurrences.
#     quad_model = [(freq[i] + 1)/ (V + trigram[1][trigram[0].index(model[i][:3])]) for i in range(len(freq))]
#     return [model, freq, quad_model]
#------------------------------------------

#Open data from file.
training_data, test_data = process_data(open_file('conference_proceedings', '.txt'))
#Word frequencies and n-gram models
#   Unigram counts
print('Making unigram model.')
unigram = unigram_model(training_data)
#   Bigram counts
print('Making bigram model.')
bigram = bigram_model(training_data)
#   Trigram counts.
print('Making trigram model.')
trigram = trigram_model(training_data)
#   Quadrigram counts.
print('Making quadrigram model.')
quadrigram = quadgram_model(training_data)
#Make those models into large lists.
models = [unigram[0], bigram[0], trigram[0], quadrigram[0]]
counts = [unigram[1], bigram[1], trigram[1], quadrigram[1]]
#Save all these models to binary files, so we don't have to run them thousands of time.
print('Saving models to file.')
save_to_file('models', [models, counts])
#And of course, return Arnold Schwarzenegger's quote.
print('YOU ARE TERMINATED!')