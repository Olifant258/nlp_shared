__author__ = 'Rick Groenendijk'

import os
import pickle
import math
import xlwt

#Function to open the data from the file and return it as the rough data.
def open_file(filePath, pkl=True):
    #Get relative path to conference proceedings folder and select the file as a filename;
    fileDir = os.path.dirname(os.path.realpath('__file__'))
    #Open the file, read out its contents and then close the file;
    if pkl == True:
        #Get path to file.
        filename = os.path.join(fileDir, 'conference_proceedings\\' + filePath + '.pkl')
        #Open the file, pkl format.
        file = open(filename, 'rb')
        #Load the data.
        text = pickle.load(file)
        file.close()
    else:
        filename = os.path.join(fileDir, 'conference_proceedings\\' + filePath + '.txt')
        file = open(filename, encoding='latin')
        text = file.read()
        file.close()
    #and return the text data.
    return text

#Function to pre-process all data (tokenize into words). Returns training and test data.
def process_data(text):
    #Split up in matrix for each \n and \t in the document. Remain only with the titles of the papers.
    text = [line.split('\t')[2] for line in text.split('\n')]
    #Format the data, so that we remain only with a list of lists of tokenized sentences.
    data = []
    lineMov = '.,!?:;()-/+' #Special characters to be removed.
    for line in text:
        line = line.lower() #Make all letters low cases.
        for i in range(len(lineMov)): #Remove special characters and replace them with a white space.
            line = line.replace(lineMov[i], ' ')
        line = '<bos> ' + line + ' <eos>' #Add tokens for the start of the sentence and the end of the sentence.
        line = line.split()
        data.append(line)
    #Get relative path to stanford models folder and select the file as a filename (stopwords);
    fileDirStop = os.path.dirname(os.path.realpath('__file__'))
    filenameStop = os.path.join(fileDirStop, 'conference_proceedings\\stop.txt')
    #Open the file, read out its contents (stopwords) and then close the file;
    file = open(filenameStop, encoding='latin')
    stop = file.read()
    file.close()
    #Split the stopwords over \n.
    stop = stop.split('\n')
    #Filter the text based on the stopwords.
    data = [[w for w in line if not w in stop] for line in data]
    #Make training data and test data
    training_data = data[:19999]
    test_data = data[20000:]
    return training_data, test_data

#Function to normalise the SCP score.
def normalise(input):
    output = 2 / (1 + math.e ** -input)
    return output

#Function to compute the probability of any segment up to 4 words.
def probs(segment):
    l = len(segment)
    if l == 0:
        p = 0
    elif l == 1:
        if segment in models[0]:
            p = counts[0][models[0].index(segment)] / sum(counts[0])
        else:
            p = 1 / sum(counts[0])
    else:
        if segment in models[l - 1]:
            p = (counts[l - 1][models[l - 1].index(segment)] + 1) / (counts[l - 2][models[l - 2].index(segment[:(l - 1)])] + len(counts[l - 2]))
        else:
            if segment[:(l - 1)] in models[l - 2]:
                p = 1 / (counts[l - 2][models[l - 2].index(segment[:(l - 1)])] + len(counts[l - 2]))
            else:
                p = 1 / len(counts[l - 2])
    return p

#Function to compute the Symmetrical Conditional Probability (SCP) of a segment.
def normalized_scp(segment):
    #Length 1 pmi.
    l = len(segment)
    if l == 1:
            score = normalise(2 * math.log(probs(segment)))
    #Other lengths.
    else:
        #Nominator part of the pmi equation.
        top = probs(segment)
        #Loop over the terms in the denominator.
        bot = 0
        for i in range(1, l):
            bot += (probs(segment[:i]) * probs(segment[i:]))
        bot /= (len(segment) - 1)
        #Now divide these two terms and return the log. Then normalise that.
        score = normalise(math.log(top/bot))
    #Return the score.
    L = len(segment) / (len(segment) + 1)
    score *= L
    return score

#Function to search for the optimal segmentation of a sentence.
def segment(A, e=5, u=4):
    A = A[1:(len(A)-1)]
    #Give the length of the sentence.
    l = len(A)
    #Loop over all words in the sentence A.
    S = []
    for i in range(1, l+1):
        #Store segmentation up to word i in a new sub-list of S.
        S.append([])
        s = A[:i]
        if i <= u:
            C = normalized_scp(s)
            S[i - 1].append([[' '.join(s)], C])
        #Now loop over all previous segmentations given in S.
        for j in range(1, i):
            if i - j <= u:
                s2 = A[j:i]
                C = normalized_scp(s2)
                #Look up previous segmentations in list S and concatenate those and s2. Sum their values for C.
                for k in range(len(S[j - 1])):
                    S[i - 1].append([sum([S[j - 1][k][0] + [' '.join(s2)]], []), S[j - 1][k][1] + C])
            #Sort the list Si and keep only the top e elements.
            S[i - 1].sort(key=lambda x: x[1], reverse=True)
            if len(S[i - 1]) > e:
                S[i - 1] = S[i - 1][:e]
    #Return the highest element in S[l] as the optimal segmentation.
    return S[i - 1][0]

#Open text and tokenize the data.
print('Opening and preprocessing data...')
text = open_file('conference_proceedings', pkl=False)
training_data, test_data = process_data(text)
#Word frequencies and n-gram models, load from files. Load these into 1 list.
models, counts = open_file('models')

#Create a book and 2 sheets to save the data to.
book = xlwt.Workbook(encoding='latin')
sheet1 = book.add_sheet("Proposed segmentation")
#Create the headers in the sheet.
sheet1.write(0,0, 'Sentence ID')
sheet1.write(0,1, 'Proposed Segmentation')

#Now do all segmentation for the test data.
print('Starting segmentation...')
for i in range(len(test_data)):
    sheet1.write(i+1, 0, 20000+i)
    seg = segment(test_data[i])
    sheet1.write(i+1, 1, "['" + "' ,'".join(seg[0]) + "']")
    if i % 100 == 0:
        print('progress:', i, 'titles.')

#Save sheet to file, which is the deliverable for this assignment.
book.save('assignment4_Romme&Groenendijk.xls')
print('YOU ARE TERMINATED!')