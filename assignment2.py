__author__ = 'RickG'

import os
import xlwt
import math

#Function to open the data from the file and return it as the rough data.
def open_file():
    #Get relative path to the conference proceedings folder and select the file as a filename;
    fileDir = os.path.dirname(os.path.realpath('__file__'))
    filename = os.path.join(fileDir, 'conference_proceedings\\conference_proceedings.txt')

    #Open the file, read out its contents and then close the file;
    file = open(filename, encoding='latin')
    text = file.read()
    file.close()

    #and return the text data.
    return text

#Function to pre-process all data (tokenize into words). Returns training and test data.
def process_data(text):
    #Split up in matrix for each \n and \t in the document. Remain only with the titles of the papers.
    text = text.split('\n')
    temp = []
    for line in text:
        line = line.split('\t')
        temp.append(line[2])

    #Format the data, so that we remain only with a list of lists of tokenized sentences.
    data = []
    lineMov = '.,!?:;()-/' #Special characters to be removed.
    for line in temp:
        line = line.lower() #Make all letters low cases.
        for i in range(len(lineMov)): #Remove special characters and replace them with a white space.
            line = line.replace(lineMov[i], ' ')
        line = '<bos> ' + line + ' <eos>' #Add tokens for the start of the sentence and the end of the sentence.
        line = line.split() #Split along tokens
        data.append(line)

    #Make training data and test data
    training_data = data[:19999]
    test_data = data[20000:]
    return training_data, test_data

#Function to make a list of the frequencies of all words.
def freq_word(data):
    word = ['<bos>', '<eos>']
    freq = [len(data), len(data)]
    counter = 0
    for line in data:
        counter += 1
        if counter % 2000 == 0:
            print('word line count:', counter)
        for i in range(1, (len(line)- 1)):
            if line[i] not in word:
                word.append(line[i])
                freq.append(1)
            else:
                freq[word.index(line[i])] += 1
    return word, freq

#Function to make a list of all bigram in the data. Returns the bigrams and their frequencies.
def bigram_model(data):
    model = []
    freq = []
    counter = 0
    #Loop through all lines in the data.
    for line in data:
        #Keeping track if checked lines.
        counter += 1
        if counter % 2000 == 0:
            print('bigram line count:', counter)
        #Loop through the lines and put the bigrams in the model.
        for i in range(1, len(line)):
            new_entry = [line[i], line[i - 1]]
            if new_entry not in model:
                model.append(new_entry)
                freq.append(1)
            else:
                freq[model.index(new_entry)] += 1
    return model, freq

#Function to make a list of all trigram in the data. Returns the trigrams and their frequencies.
def trigram_model(data):
    model = []
    freq = []
    counter = 0
    #Loop through all lines in the data.
    for line in data:
        #Keeping track if checked lines.
        counter += 1
        if counter % 2000 == 0:
            print('trigram line count:', counter)
        #Check whether the line actually is longer than 3 words.
        if len(line) < 3:
            continue
        for i in range(2, len(line)):
            new_entry = [line[i], line[i - 1], line[i - 2]]
            if new_entry not in model:
                model.append(new_entry)
                freq.append(1)
            else:
                freq[model.index(new_entry)] += 1
    return model, freq

#Function to make a list of all quadgram in the data. Returns the quadgrams and their frequencies.
def quadgram_model(data):
    model = []
    freq = []
    counter = 0
    #Loop through all lines in the data.
    for line in data:
        #Keeping track if checked lines.
        counter += 1
        if counter % 2000 == 0:
            print('quadgram line count:', counter)
        #Check whether the line actually is longer than 4 words.
        if len(line) < 4:
            continue
        for i in range(3, len(line)):
            new_entry = [line[i], line[i - 1], line[i - 2], line[i - 3]]
            if new_entry not in model:
                model.append(new_entry)
                freq.append(1)
            else:
                freq[model.index(new_entry)] += 1
    return model, freq

#Function to calculate maximum likelihood probabilities using add-one smoothing.
def max_likelihood(words, wfreq, model, mfreq, bigram=True):
    #List for all probabilities of bigrams.
    probs = []
    #Vocabulary V of all words.
    V = len(words)
    #Loop over all bigrams.
    counter = 0
    if bigram == True:
        for grams in model:
            cv = mfreq[counter]
            csum = V + wfreq[words.index(grams[1])]
            p = cv/csum
            probs.append(p)
            counter += 1
    if bigram == False:
        for grams in model:
            cv = mfreq[counter]
            csum = V + wfreq[words.index(grams[1:])]
            p = cv/csum
            probs.append(p)
            counter += 1
    return probs

#Function to apply bigram models to test data and return the log probability of each sentence in the data.
def log_probability(words, wfreq, model, mfreq, data):
    #List for all probabilities of bigrams.
    probs = []
    #Vocabulary V of all words.
    V = len(words)
    #Return all bigrams in the test-data.
    bi_test = []
    for line in data:
        for i in range(1, len(line)):
            bi_test.append([line[i], line[i - 1]])
    #Loop of all bigrams in the test data and return the individual probability for each bigram.
    for grams in bi_test:
        #Save all probabilities of bigrams to temp.
        if grams[1] == '<bos>':
            temp = []
        #Amount of occurrences of the bigram in the training data, plus add-one smoothing.
        cv = 1
        if grams in model:
            cv = cv + mfreq[model.index(grams)]
        #Amount of times the word occurs in V, adding V.
        csum = V
        if grams[1] in words:
            csum = csum + wfreq[words.index(grams[1])]
        #10-log of the maximum likelihood probability and append it to temp.
        temp.append(math.log10(cv/csum))
        #If the end of the sentence is reached, push the summed log probability to probs.
        if grams[0] == '<eos>':
            probs.append(sum(temp))
    return probs

#Function to make an ordered list of the 50 most-used n-grams.
def max_grams(model, prob):
    gram_list = []
    #Loop 50 times to find the 50 most used items.
    for i in range(50):
        m_index = prob.index(max(prob))
        gram_list.append([model[m_index], prob[m_index]])
        prob[m_index] = -1
    return gram_list

#Function to return an ordered (descending ranked) list of all sentences in the testing data
def min_grams(data, prob):
    s_list = []
    #Loop over the list for the amount of items in the test data.
    for i in range(len(data)):
        m_index = prob.index(max(prob))
        s_list.append([data[m_index], prob[m_index]])
        prob[m_index] = -1000
    return s_list

#Create a book and 2 sheets to save the data to.
book = xlwt.Workbook(encoding='latin')
sheet1 = book.add_sheet("top 50 n-gram frequencies")
sheet2 = book.add_sheet("bigram model on titles")
#Create the headers in the sheet.
#Sheet 1.
sheet1.write(0,0, 'bigrams')
sheet1.write(1,0, 'w')
sheet1.write(1,1, 'w -1')
sheet1.write(1,2, 'max. lik. prob.')

sheet1.write(0,3, 'trigrams')
sheet1.write(1,3, 'w')
sheet1.write(1,4, 'w -1')
sheet1.write(1,5, 'w -2')
sheet1.write(1,6, 'max. lik. prob.')

sheet1.write(0,7, 'quadgrams')
sheet1.write(1,7, 'w')
sheet1.write(1,8, 'w -1')
sheet1.write(1,9, 'w -2')
sheet1.write(1,10, 'w -3')
sheet1.write(1,11, 'max. lik. prob.')
#Sheet 2.
sheet2.write(0,0, 'titles')
sheet2.write(0,1, 'log probability')

#Open data from file.
text = open_file()
training_data, test_data = process_data(text)

#Word frequencies and n-gram models
m2, mf2 = bigram_model(training_data)
m3, mf3 = trigram_model(training_data)
m4, mf4 = quadgram_model(training_data)
w, wf = freq_word(training_data)
print('Finished computing all frequencies of n-grams.')

#Maximum probabilities for n-gram words.
p2 = max_likelihood(w, wf, m2, mf2)
p3 = max_likelihood(m2, mf2, m3, mf3, bigram=False)
p4 = max_likelihood(m3, mf3, m4, mf4, bigram=False)
print('Finished 3 maximum likelihood models.')

#Log probabilities over the test data, with the bigram model as input.
p_log = log_probability(w, wf, m2, mf2, test_data)
s_list = min_grams(test_data, p_log)
print('Finished log probability descending ordered list of titles in test data.')

#Look up the 50 best n-grams in the probability list.
r2 = max_grams(m2, p2)
r3 = max_grams(m3, p3)
r4 = max_grams(m4, p4)
print('Finished ordering the 50 best n-grams.')

#Now write the top 50 combinations to sheet 1.
for i in range(50):
    #Bigrams.
    sheet1.write(i+2, 0, r2[i][0][1])
    sheet1.write(i+2, 1, r2[i][0][0])
    sheet1.write(i+2, 2, r2[i][1])
    #Trigrams
    sheet1.write(i+2, 3, r3[i][0][2])
    sheet1.write(i+2, 4, r3[i][0][1])
    sheet1.write(i+2, 5, r3[i][0][0])
    sheet1.write(i+2, 6, r3[i][1])
    #Quadgrams
    sheet1.write(i+2, 7, r4[i][0][3])
    sheet1.write(i+2, 8, r4[i][0][2])
    sheet1.write(i+2, 9, r4[i][0][1])
    sheet1.write(i+2, 10, r4[i][0][0])
    sheet1.write(i+2, 11, r4[i][1])

#Lastly, write a descending list of titles from the test data to sheet 2.
for i in range(len(s_list)):
    ns = ''
    for word in s_list[i][0]:
        ns = ns + " " + word
    sheet2.write(i+1, 0, ns)
    sheet2.write(i+1, 1, s_list[i][1])

#Save sheet to file, which is the deliverable for this assignment.
book.save('assignment2_Romme&Groenendijk.xls')
print('YOU ARE TERMINATED!')