from xml.dom.minidom import parse
import xml.dom.minidom
import time

def sublist(sublst,lst):
    n = len(sublst)
    return any((sublst == lst[i:i+n]) for i in xrange(len(lst)-n+1))


t= time.time()
def worddist(word1, word2):
        count = 0
        word1=str(word1)
        word2=str(word2)
        if (len(word1) > len(word2)):
                word3 = word1
                word1 = word2
                word2 = word3
        if (sublist(word1,word2)==1):
                dist = -1
                return dist
        for i in range(len(word1)):
                if word1[i] == word2[i]:
                        count +=1  
        dist = len(word1)-count
        return dist


def CDlist(ID, CID, Artist, Title, Category, Tracks):
        artists = []
        for i in range(len(Artist)):
                for j in range(len(Artist)):
                        if worddist(Artist[i], Artist[j]) <2 and i!=j:
                                artists.append([i,j, worddist(Artist[i], Artist[j])])
        titels = []
        for i in range(len(Title)):
                for j in range(len(Title)):
                        if worddist(Title[i], Title[j]) <1 and i!=j:
##                                print(i,j)
##                                print(worddist(Artist[i], Artist[j]))
                                titels.append([i,j, worddist(Title[i], Title[j])])
##        print(len(titels))
##        combi = (tuple((tuple(sorted(titels[i])) for i in range(len(titels)) if titels[i] in artists)))
##        combi = set(combi)
        outcomes = []
        used = []
        for i in range(len(artists)):
                hoi = False
                for j in range(len(titels)):
                        if artists[i][0] == titels[j][0]:
                                if artists[i][1] == titels[j][1]:
                                        used.append(j)
                                        outcomes.append([artists[i][0], artists[i][1], artists[i][2],titels[j][2]])
                                        hoi = True
                if hoi == False:
                      outcomes.append([artists[i][0], artists[i][1], artists[i][2], 10])
        for i in range(len(titels)):
                if i not in used:
                        outcomes.append([titels[j][0], titels[j][1], titels[j][2], 8])
                      
                
##        doubles = [y for [x,y] in combi]
##        print(len(doubles))
##        newartists = [[(ID[i], Artist[i], Title[i], Category[i],Tracks[i]), ] for i in range(len(ID)) if i not in doubles]
        return outcomes

def prepro(input):
        input = [word.lower() for word in input]
        return input
Discs = [("ID"), ("Cid"), ("Artist"), ("Dtitle"), ("Category"), ("Tracks")]

ID = []
CID = []
Artist = []
Dtitle = []
Category = []
Tracks = []
# Open XML document using minidom parser
DOMTree = xml.dom.minidom.parse("discs2.xml")
discs1 = DOMTree.documentElement

### Get all the discs in the collection
discs = discs1.getElementsByTagName("disc")

a=0


# Print detail of each movie.
for disc in discs:
   a+=1
##   print("*****Disc*****")

   id = disc.getElementsByTagName('id')[0]
##   print("Id: %s" % id.childNodes[0].data)
   ID.append(id.childNodes[0].data)
   cid = disc.getElementsByTagName('cid')[0]
##   print ("Cid: %s" % cid.childNodes[0].data)
   cid1 = (cid.childNodes[0].data).split(',')
   CID.append(cid1)
   artist = disc.getElementsByTagName('artist')[0]
##   print ("Artist: %s" % artist.childNodes[0].data)
   
   Artist.append(prepro([artist.childNodes[0].data]))
   dtitle = disc.getElementsByTagName('dtitle')[0]
##   print ("Dtitle: %s" % dtitle.childNodes[0].data)
   Dtitle.append(prepro([dtitle.childNodes[0].data]))
   category = disc.getElementsByTagName('category')[0]
##   print ("Category: %s" % category.childNodes[0].data)
   Category.append(prepro([category.childNodes[0].data]))
   tracks = disc.getElementsByTagName("title")
   liedjes = []
   for i in range (tracks.length):
       liedjes.append(prepro([tracks.item(i).childNodes[0].data]))
##       print ("Track",i, ": %s" % tracks.item(i).childNodes[0].data)
   Tracks.append(liedjes)

def trans(text):
    try:
       text = text.encode('utf')
       return text
    except UnicodeEncodeError:
        return text

for i in range(len(Artist)-1):
        worddist(Artist[i],Artist[i+1])
##      print(Artist[i])
##      print(Artist[i+1])
##      print(worddist(Artist[i],Artist[i+1]))

text_file = open("Output.txt", "w")
for number in ID:
   text_file.write("disc_id(%s" % number + ").\n")

text_file.write( "\n")
for i in range(len(ID)):
   for o in range(len(CID[i])):
      text_file.write("disc_cid(%s, \"%s\"" % (ID[i], CID[i][o]) + ").\n")

text_file.write( "\n")
for i in range(len(ID)):
      artiestje = Artist[i][0]
      

      artiestje = artiestje.encode('ascii', 'xmlcharrefreplace')
      artiestje = (artiestje.decode('ascii', 'ignore'))

      text_file.write("disc_artist(%s, \"%s\"" % (ID[i], artiestje) + ").\n")
text_file.write( "\n")
for i in range(len(ID)):

   titel = Dtitle[i][0].encode('ascii', 'xmlcharrefreplace').decode('ascii', 'ignore')
   text_file.write("disc_dtitle(%s, \"%s\"" % (ID[i], titel) + ").\n")
text_file.write( "\n")
for i in range(len(ID)):

      titel = Category[i][0]
      text_file.write("disc_category(%s, \"%s\"" % (ID[i], titel) + ").\n")

text_file.write( "\n")

for i in range(len(ID)):
   tracks = []
   for o in range(len(Tracks[i])):
      tracks.append(Tracks[i][o][0].encode('ascii', 'xmlcharrefreplace').decode('ascii', 'ignore'))
      
   text_file.write("disc_track(%s, \"%s\"" % (ID[i], tracks) + ").\n")

text_file.close()

x = CDlist(ID, CID, Artist, Dtitle, Category, Tracks)

Cd = [[ID[i], CID[i], Artist[i], Dtitle[i], Category[i], Tracks[i],1] for i in range(len(ID))]

for i in range(len(x)):
        k1 = 1
        k2 = 0
        if x[i][2] == -1:
                if x[i][3] == -1:
                        k1 = 0.2
                        k2 = 0.8

                if x[i][3] == 1:
                        k1 = 0.3
                        k2 = 1-k1

                if x[i][3] == 2:
                        k1 = 0.4
                        k2 = 1-k1

                if x[i][3] == 0:
                        k1 = 0.1
                        k2 = 1-k1

                if x[i][3] == 8:
                        k1 = 0.8
                        k2 = 1-k1

                if x[i][3] == 10:
                        k1 = 0.8
                        k2 = 1-k1

        if x[i][2] == 0:

                if x[i][3] == -1:
                        k1 = 0.05
                        k2 = 1-k1

                if x[i][3] == 1:
                        k1 = 0.3
                        k2 = 1-k1

                if x[i][3] == 2:
                        k1 = 0.4
                        k2 = 1-k1

                if x[i][3] == 0:
                        k1 = 0
                        k2 = 1-k1

                if x[i][3] == 8:
                        k1 = 0.7
                        k2 = 1-k1

                if x[i][3] == 10:
                        k1 = 0.7
                        k2 = 1-k1

        if x[i][2] == 1:

                if x[i][3] == -1:
                        k1 = 0.3
                        k2 = 1-k1

                if x[i][3] == 1:
                        k1 = 0.4
                        k2 = 1-k1

                if x[i][3] == 2:
                        k1 = 0.5
                        k2 = 1-k1

                if x[i][3] == 0:
                        k1 = 0.2
                        k2 = 1-k1

                if x[i][3] == 8:
                        k1 = 0.8
                        k2 = 1-k1

                if x[i][3] == 10:
                        k1 = 0.8
                        k2 = 1-k1

        if x[i][2] == 2:

                if x[i][3] == -1:
                        k1 = 0.4
                        k2 = 1-k1

                if x[i][3] == 1:
                        k1 = 0.5
                        k2 = 1-k1

                if x[i][3] == 2:
                        k1 = 0.6
                        k2 = 1-k1

                if x[i][3] == 0:
                        k1 = 0.3
                        k2 = 1-k1

                if x[i][3] == 8:
                        k1 = 0.9
                        k2 = 1-k1

                if x[i][3] == 10:
                        k1 = 0.9
                        k2 = 1-k1
        Cd[x[i][0]][6] = Cd[x[i][0]][6]*k1
        Cd[x[i][1]][6] = Cd[x[i][1]][6]*k1
        Cd.append(Cd[x[i][0]])
        Cd[-1][6] = k2
for cd in Cd:
    print(cd[6])
