__author__ = 'RickG'

import os
import xlwt
from collections import Counter
import time

#Function to open the data from the file and return it as the rough data.
def open_file(filePath, fileType):
    #Get relative path to the Wimbledon Tweets folder and select the file as a filename;
    fileDir = os.path.dirname(os.path.realpath('__file__'))
    filename = os.path.join(fileDir, 'Wimbledon2014\\' + filePath + fileType)
    #Open the file, read out its contents and then close the file;
    file = open(filename, encoding='latin')
    text = file.read()
    file.close()
    #and return the text data.
    return text

#Open the text from the file. Split up the text at the end of each line. We are left with a list of lines.
text = [line[49:] for line in open_file('Wimbledon2014', '.txt').split('\n')]
#Process the data, so as to split over the amount of words.
print('Opened data. Now preprocessing all data.')
st = time.time()
lineMov = '.,!?:;'
for i in range(len(text)):
    for j in range(len(lineMov)):
        text[i] = text[i].replace(lineMov[j], '')
    text[i] = (text[i].lower()).split()
print('Preprocessed data in', (time.time() - st), 'seconds. On to processing hashtags and mentions.')

#Flatten the list of sub-lists.
words = [item for sublist in text for item in sublist if item[0] == '#' or item[0] == '@']
#Make a dictionary for all words and their frequencies.
dic = [[key,value] for key, value in Counter(words).items()]
#Now split along hashtags and mentions.
hashtags = [item for item in dic if item[0][0] == '#']
mentions = [item for item in dic if item[0][0] == '@']

#Sort hashtags and mentions.
hashtags.sort(key=lambda x: x[1], reverse=True)
mentions.sort(key=lambda x: x[1], reverse=True)
#Now only take the top 20 hashtags and mentions.
hashtags = hashtags[:20]
mentions = mentions[:20]
print('Counted and sorted all entities. Now saving to file.')

#Create a book and sheet to save the data to.
book = xlwt.Workbook(encoding='latin')
sheet1 = book.add_sheet("sheet 1")
#Create the headers in the sheet.
sheet1.write(0,0,'hashtags')
sheet1.write(0,1,'frequency')
sheet1.write(0,2,'mentions')
sheet1.write(0,3,'frequency')
#Now write the data to the file.
for i in range(20):
    sheet1.write(i+1, 0, hashtags[i][0])
    sheet1.write(i+1, 1, hashtags[i][1])
    sheet1.write(i+1, 2, mentions[i][0])
    sheet1.write(i+1, 3, mentions[i][1])
#Save sheet to file, which is the deliverable for this assignment.
book.save('assignment1_Romme&Groenendijk.xls')
print('YOU ARE TERMINTAED!')