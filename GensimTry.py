"""
Created on Tue Mar 15 14:12:14 2016

@author: Mark
"""
from __future__ import division
from operator import itemgetter
from sklearn.neighbors import NearestNeighbors
import os
import pickle
import math
#import xlwt
import collections
import random
import time

import logging

import gensim
import numpy as np
from gensim import corpora, models, similarities

from stemming.porter2 import stem

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
#Function to open the data from the file and return it as the rough data.
def open_file(filePath, pkl=True):
    #Get relative path to conference proceedings folder and select the file as a filename;
    fileDir = os.path.dirname(os.path.realpath('__file__'))
    #Open the file, read out its contents and then close the file;
    if pkl == True:
        #Get path to file.
        filename = os.path.join(fileDir, filePath + '.pkl')
        #Open the file, pkl format.
        file = open(filename, 'rb')
        #Load the data.
        text = pickle.load(file)
        file.close()
    else:
        filename = os.path.join(fileDir, filePath + '.txt')
        file = open(filename)
        text = file.read()
        file.close()
    #and return the text data.
    return text
    
#Function to pre-process all data (tokenize into words). Returns training and test data.
def process_data(text):
    
    #Split up in matrix for each \n and \t in the document. Remain only with the titles of the papers.
    text = [line.split('\t') for line in text.split('\n')]
    #Format the data, so that we remain only with a list of lists of tokenized sentences.
    dataid = []
    datacategory = []
    dataline = []
    lineMov = '.,!?:;()-/\+<>' #Special characters to be removed.
    for line in text:
        if len(line) > 1:
            id = line[0]
            category = line[1]
            line = line[2:]
            line = line[0].lower() #Make all letters low cases.
            for i in range(len(lineMov)): #Remove special characters and replace them with a white space.
                line = line.replace(lineMov[i], ' ')
            #line = '<bos> ' + line + ' <eos>' #Add tokens for the start of the sentence and the end of the sentence.
            line = line.split()
    
            dataid.append(id)
            datacategory.append(category)
            
            dataline.append(line)
    from collections import defaultdict
    frequency = defaultdict(int)
    for text in dataline:
        for token in text:
            frequency[token] += 1
    
    dataline = [[token for token in text if frequency[token] > 1] for text in dataline]
    #Get relative path to stanford models folder and select the file as a filename (stopwords);
    fileDirStop = os.path.dirname(os.path.realpath('__file__'))
    filenameStop = os.path.join(fileDirStop, 'conference_proceedings\\stop.txt')
    #Open the file, read out its contents (stopwords) and then close the file;
    file = open(filenameStop)
    stop = file.read()
    file.close()
    #Split the stopwords over \n.
    stop = stop.split('\n')
    #Filter the text based on the stopwords.
    dataline = [[w for w in line if not w in stop if w >1] for line in dataline]
    #Make training data and test data
    dataas = zip(dataid, datacategory, dataline)
    training_data = dataas[:19999]
    test_data =dataas[20000:]
    return training_data, test_data


def sent2vec(sentence):
    count = 0
    matrix = []
    for word in sentence:
        if word in model:
            matrix.append(model[word])
    vecrep = np.sum( np.array(matrix), axis=0 )
    vecrep = vecrep.tolist
    return vecrep

def mean(a):
    return sum(a) / len(a)

def accuracy(test, outcome):
    counter = 0
    for i in range(len(test)):
        if (test[i] == outcome[i]):
            counter += 1
    uitslag = float(counter)/float((len(test)))
    return uitslag

def euclideanDistance(instance1, instance2, length):
    distance = 0
    
    for x in range(length):
        
        print(instance1[x], instance2[x])
        distance += pow((instance1[x] - instance2[x]), 2)
    print(distance)
    return math.sqrt(distance)

def getDict(sentences):
    dictionary = corpora.Dictionary(sentences)
    
    dictionary.save('/deerwester.dict') # store the dictionary, for future reference
    return dictionary
    
#Open text and tokenize the data.
print('Opening and preprocessing data...')
text = open_file('Data', pkl=False)
data, dat = process_data(text)
sentences = [sentence for [id, cid, sentence]in data]


num_features = 500    # Word vector dimensionality                      
min_word_count = 5   # Minimum word count                        
num_workers = 4       # Number of threads to run in parallel
context = 10          # Context window size                                                                                    
downsampling = 1e-3   # Downsample setting for frequent words

from gensim.models import word2vec
print "Training model..."
model = word2vec.Word2Vec(sentences, workers=num_workers, \
            size=num_features, min_count = min_word_count, \
            window = context, sample = downsampling)

# If you don't plan to train the model any further, calling 
# init_sims will make the model much more memory-efficient.
model.init_sims(replace=True)

# It can be helpful to create a meaningful model name and 
# save the model for later use. You can load it later using Word2Vec.load()
model_name = "300features_40minwords_10context"
model.save(model_name)
categories = [cid for [id, cid, sentence]in data]
testsentences = [sentence for [id, cid, sentence]in dat]
testcategories = [cid for [id, cid, sentence]in dat]
ids= [id for [id, cid, sentence]in data]

newsentences = []

for sentence in sentences:
    words = []
    
    
    for word in sentence:
        if word in model:
            words.append(model[word].tolist())
    newsentences.append(words)

##ns = list(newsentences)
##    
##newsentences = [[vec[word]] for sentence in sentences for word in sentence if word is not '<bos>' if word is not '<eos>']

newtestsentences = []

for sentence in testsentences:
    words = []
    
    
    for word in sentence:
        if word in model:
            words.append(model[word].tolist())
    newtestsentences.append(words)

##print(type(newtestsentences))
##nts = np.array(newtestsentences.tolist())

Output = [[categories[i],newsentences[i]] for i in range(len(ids))]
Output2 = [[testcategories[i],newtestsentences[i]] for i in range(len(testcategories))]
Final = Output + Output2

text_file = open("Final.txt", "w")
for [id, sent] in Final:
    text_file.write(id + str(sent) + ".\n")
##print(model)
####
####dictionary = getDict(sentences)
####
####zinnen = [', '.join(sentence) for sentence in sentences]
####
####model = gensim.models.Doc2Vec(zinnen, size=100, window=8, min_count=5, workers=4)
##
####
####model.infer_vector(NewDocument)
##
##
##k_means = [[] for i in range(5)]
##count1 = 0
##count2 = 0
##count3 = 0
##count4 = 0
##count5 = 0
##
##
##for [id, cid, sentence] in data:
##    words =[]
##    if cid == 'ISCAS':
##        i = 0
##        count1 +=1
##    if cid == 'VLDB':
##        i=1
##        count2 +=1
##    if cid == 'INFOCOM':
##        i=2
##        count3 +=1
##    if cid == 'SIGGRAPH':
##        i=3
##        count4 +=1
##    if cid == 'WWW':
##        i=4
##        count5 +=1    
##    k_means[i].append(sent2vec(sentence))
##    
##
##
##count = [count1, count2, count3, count4, count5]
##
##
##    
##for i in range(5):
##    a = np.array(k_means[i])
##    k_means[i] = np.mean(a, axis=0)
##
##k_means[0] = [k_means[0][i]/count1 for i in range(len(k_means[0]))]
##k_means[1] = [k_means[1][i]/count2 for i in range(len(k_means[1]))]
##k_means[2] = [k_means[2][i]/count3 for i in range(len(k_means[2]))]
##k_means[3] = [k_means[3][i]/count4 for i in range(len(k_means[3]))]
##k_means[4] = [k_means[4][i]/count5 for i in range(len(k_means[4]))]
##    
##categories = [cid for [id, cid, sentence]in data]
##ids= [id for [id, cid, sentence]in data]
##
##
##
##tsentences = [sentence for [id, cid, sentence]in dat]
##tcategories = [cid for [id, cid, sentence]in dat]
##tids= [id for [id, cid, sentence]in dat]
##
##
##for testsentence in tsentences:
##    vecRep = sent2vec(testsentence)
####    print(vecRep)
##    distance = []
##    for i in range(len(k_means)):
##        dist = euclideanDistance(vecRep, k_means[i], len(vecRep))          
##        distance.append([i, dist])
##    distance = sorted(distance, key=itemgetter(1))
##    t = time.time()
##    print(distance)
##    cat = [id for [id, dist] in distance]
##    mostCom = cat[0]
##    
##    outcomes.append(mostCom)
##
##for i in range(len(outcomes)):
##    if outcomes[i] ==0:
##        outcomes[i] = 'ISCAS'
##    if outcomes[i] ==1:
##        outcomes[i] = 'VLDB'
##    if outcomes[i] ==2:
##        outcomes[i] = 'INFOCOM'
##    if outcomes[i] ==3:
##        outcomes[i] = 'SIGGRAPH'
##    if outcomes[i] ==4:
##        outcomes[i] = 'WWW'
##
##print(testcids[0:10])
##print(outcomes[0:10])
##accuracy = accuracy(testcids, outcomes)
##
##print(accuracy)
##
##
##    
##
##dictionary = corpora.Dictionary(sentences)
##dictionary.save('/deerwester.dict') # store the dictionary, for future reference
##
##
##vecs = [[dictionary.doc2bow(tsentence)] for tsentence in tsentences]
##

