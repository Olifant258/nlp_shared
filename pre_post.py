__author__ = 'Rick Groenendijk'

import os
import pickle
import time
import math
import re
from collections import Counter

#Function to open the data from the file and return it as the rough data.
def open_file(filePath, pkl=True):
    #Get relative path to conference proceedings folder and select the file as a filename;
    fileDir = os.path.dirname(os.path.realpath('__file__'))
    #Open the file, read out its contents and then close the file;
    if pkl == True:
        #Get path to file.
        filename = os.path.join(fileDir, 'conference_proceedings\\' + filePath + '.pkl')
        #Open the file, pkl format.
        file = open(filename, 'rb')
        #Load the data.
        text = pickle.load(file)
        file.close()
    else:
        filename = os.path.join(fileDir, 'conference_proceedings\\' + filePath + '.txt')
        file = open(filename, encoding='latin')
        text = file.read()
        file.close()
    #and return the text data.
    return text

#Function to pre-process all data (tokenize into words). Returns training and test data.
def process_data(text):
    #Note down the classes for the titles.
    classes = [line.split('\t')[1] for line in text.split('\n')]
    #Remove special characters from the complete body of text and lower all characters.
    text = re.sub('[^A-Za-z0-9 \t\n]', ' ', text)
    text = text.lower()
    #Split up in matrix for each \n and \t in the document. Remain only with the titles of the papers.
    text = [line.split('\t')[2] for line in text.split('\n')]
    #Introduce <bos> and <eos> tokens and tokenize the lines in text.
    text = [['<bos>'] + [word for word in line.split() if len(word) > 1] + ['<eos>'] for line in text]
    #Open the file, read out its contents (stopwords) and then close the file; Then split over '\n'.
    stop = open_file('stop', '.txt').split('\n')
    #Filter the text based on the stopwords.
    text = [[w for w in line if not w in stop] for line in text]
    #Make training data and test data
    training_data = text[:19999]
    test_data = text[20000:]
    training_classes = classes[:19999]
    test_classes = classes[20000:]
    #Return both the data and the classes.
    return training_data, test_data, training_classes, test_classes

#Function to return the test accuracy for the predicted classes on the test data.
def accuracy(predict):
    #Register those words that match the actual test classes. Then divide over the length of the classes.
    accuracy = len([predict[i] for i in range(len(predict)) if predict[i] == test_classes[i]]) / len(test_classes)
    #Print for ease.
    # print('The test accuracy is', accuracy)
    return accuracy

#Function to return a number for a classes, or vice versa.
#0 for ISCAS, 1 for SIGGRAPH, 2 for WWW, 3 for INFOFCOM and 4 for VLDB.
def replacer(list, reverse=False):
    if reverse == False:
        for i in range(len(list)):
            if list[i] == 'ISCAS':
                list[i] = 0
            elif list[i] == 'SIGGRAPH':
                list[i] = 1
            elif list[i] == 'WWW':
                list[i] = 2
            elif list[i] == 'INFOCOM':
                list[i] = 3
            else:
                list[i] = 4
    else:
        for i in range(len(list)):
            if list[i] == 0:
                list[i] = 'ISCAS'
            elif list[i] == 1:
                list[i] = 'SIGGRAPH'
            elif list[i] == 2:
                list[i] = 'WWW'
            elif list[i] == 3:
                list[i] = 'INFOCOM'
            else:
                list[i] = 'VLDB'
    return list

#Function that makes a confusion matrix. Also returns precision, recall and F1 measures.
#Note that for this problem, accuracy is desired. p, r and F1 are superfluous. See chapter 8, ass3, page 183.
def performance(predict):
    #Replace terms by integers.
    predict = replacer(predict)
    classes = replacer(test_classes)
    #Make a confusion matrix for these two lists.
    conf_matrix = [[0] * 5 for i in range(5)]
    for i in range(len(predict)):
        conf_matrix[classes[i]][predict[i]] += 1
    #Calculate true positives.
    a = 0
    for i in range(5):
        a += conf_matrix[i][i]
    #Calculate false positives and false negatives (which are the same, thus sum(conf_matrix) - a.
    s = sum([sum(line) for line in conf_matrix])
    b = s - a
    c = s - a
    #Compute micro-averaged precision, recall, and F1 measure.
    p = a/(a+b)
    r = a/(a+c)
    f = (2*p*r)/(p+r)
    return p, r, f, conf_matrix

#Function to determine the distribution of classes across the data, necessary for naive bayes.
def class_distribution(classes):
    classes = replacer(classes)
    c = [0] * 5
    for i in range(len(classes)):
        c[classes[i]] += 1
    c = [x/len(classes) for x in c]
    replacer(classes, reverse=True)
    return c

#Function to make a model for unigrams, returning tokens and their counts in dictionary count.
def unigram_model(data, classes):
    #Merge lists.
    text = [list(i) for i in zip(classes, data)]
    #Make separate models for each class.
    cl = ['ISCAS', 'SIGGRAPH', 'WWW', 'INFOCOM', 'VLDB']
    dic = []
    for c in cl:
        #Flatten the list of sub-lists.
        words = [item for sublist in text for item in sublist[1] if sublist[0] == c]
        #Make a dictionary for all words and their frequencies.
        dic.append(dict([[key, value+1] for key,value in Counter(words).items()]))
    #return the list of dictionaries.
    return dic

#Do Naive-Bayes classification on the data using the multiplication of probabilities of unigrams per class.
def unigram_classifier(dic, data, classes):
    #Return the log of the class distribution array.
    classes = [math.log(x) for x in classes]
    #Loop over the lines in the data.
    predict = []
    for line in data:
        result = [0] * 5
        #Do for each class.
        for i in range(5):
            l = sum(dic[i].values())
            for w in line:
                if w in dic[i]:
                    result[i] += math.log(dic[i][w] / l)
                else:
                    result[i] += math.log(1/l)
        #Now take into account the chances of each class being in the data.
        result = [result[i] + classes[i] for i in range(len(result))]
        #Append the most likely candidate to predict.
        predict.append(result.index(max(result)))
    #Return the predict, but replace the integers by strings first.
    return replacer(predict, reverse=True)

#Function to make a list of all bigram in the data. Returns the bigrams and their frequencies.
def bigram_model(data, classes):
    #Merge lists.
    text = [list(i) for i in zip(classes, data)]
    #Make separate models for each class.
    cl = ['ISCAS', 'SIGGRAPH', 'WWW', 'INFOCOM', 'VLDB']
    dic = []
    for c in cl:
        #Flatten the list of sub-lists.
        word0 = [item for sublist in text for item in sublist[1] if sublist[0] == c]
        word1 = word0[1:]
        data = [list(i) for i in zip(word0, word1) if '<eos>' not in i[0]]
        #Make a dictionary for all words and their frequencies.
        dic.append(dict([[(a, b), v+1] for (a, b), v in Counter(map(tuple, data)).items()]))
    #return the model and frequencies.
    return dic

#Do Naive-Bayes classification on the data using the multiplication of probabilities of bigrams per class.
def bigram_classifier(bi_dic, uni_dic, data, classes):
    #Return the log of the class distribution array.
    classes = [math.log(x) for x in classes]
    #Loop over the lines in the data.
    predict = []
    for line in data:
        result = [0] * 5
        for i in range(5):
            V = len(uni_dic[i].values())
            for j in range(len(line) - 1):
                if (line[j], line[j + 1]) in bi_dic[i]:
                    result[i] += math.log(bi_dic[i][(line[j], line[j + 1])] / (uni_dic[i][line[j]] + V))
                else:
                    if line[j] in uni_dic[i]:
                        result[i] += math.log(1 / (uni_dic[i][line[j]] + V))
                    else:
                        result[i] += math.log(1 / V)
        #Now take into account the chances of each class being in the data.
        result = [result[i] + classes[i] for i in range(len(result))]
        #Append the most likely candidate to predict.
        predict.append(result.index(max(result)))
    return replacer(predict, reverse=True)

t = time.time()
#Open text and pre-process the data.
text = open_file('conference_proceedings', pkl=False)
training_data, test_data, training_classes, test_classes = process_data(text)

#Compute the distribution of classes in the training_data.
class_dist = class_distribution(training_classes)

#----Naive-Bayes Unigram-Based------------
uni = unigram_model(training_data, training_classes)
pred = unigram_classifier(uni, test_data, class_dist)
print('Unigram-based Naive-Bayes classifier accuracy is:', accuracy(pred))
#-----------------------------------------

#----Naive-Bayes Bigram-Based-------------
uni = unigram_model(training_data, training_classes)
bi = bigram_model(training_data, training_classes)
pred = bigram_classifier(bi, uni, test_data, class_dist)
print('Bigram-based Naive-Bayes classifier accuracy is:', accuracy(pred))
#-----------------------------------------

print('Elapsed time:', time.time() - t)


print('YOU ARE TERMINATED!')
