from xml.dom.minidom import parse
from nltk.corpus import wordnet as wn
from itertools import combinations

import xml.dom.minidom
import time
import name_tools
from difflib import SequenceMatcher
from collections import Counter

def similar(a, b):
    return SequenceMatcher(None, a, b).ratio()



def worddist(word1, word2):
    word1=(word1[0])
    word2=(word2[0])
    
    prob = similar(word1,word2)
    if prob < 0.7:
        prob = 0
    return prob


def CDlist(ID, CID, Artist, Title, Category, Tracks):
        tel = 0
        artists = []
        tell2 = 0
        for i in range(len(Artist)):
                for j in range(len(Artist)):
                        if worddist(Artist[i], Artist[j]) > 0 and i!=j:
                            if worddist(Title[i], Title[j]) > 0:
                                tell2 +=1
##                                for track1 in Tracks[i]:
##                                    count = 0
##                                    for track2 in Tracks[j]:
##                                        if worddist(track1,track2)>0:
##                                            count+=1
##                                            #print(track1,track2)
##                                    if count>= len(Tracks[i]):
##                                        tel +=1
##                                artists.append([i,j, worddist(Artist[i], Artist[j])])
        print(tel)
        print(tell2)
        titels = []
        for i in range(len(Title)):
                for j in range(len(Title)):
                        if worddist(Title[i], Title[j]) > 0 and i!=j:
                            print(Title[i], Title[j])
                            titels.append([i,j, worddist(Title[i], Title[j])])
        outcomes = []
        used = []
        for i in range(len(artists)):
                hoi = False
                for j in range(len(titels)):
                        if artists[i][0] == titels[j][0]:
                                if artists[i][0] == titels[j][0] and artists[i][1] == titels[j][1]:
                                        used.append([titels[j][0],titels[j][1]])
                                        outcomes.append([artists[i][0], artists[i][1], float(artists[i][2]*titels[j][2])])
                                        hoi = True
                if hoi == False:
                      outcomes.append([artists[i][0], artists[i][1], artists[i][2]])
        outcomes.append("hoi")
        for [x,y,z] in titels:
            if [x,y] not in used:
                outcomes.append([x,y,z])
                      
                
##        doubles = [y for [x,y] in combi]
##        print(len(doubles))
##        newartists = [[(ID[i], Artist[i], Title[i], Category[i],Tracks[i]), ] for i in range(len(ID)) if i not in doubles]
        return outcomes

def prepro(input):
        input = [word.lower() for word in input]
        return input

def prepros():
    Discs = [("ID"), ("Cid"), ("Artist"), ("Dtitle"), ("Category"), ("Tracks")]
    ID = []
    CID = []
    Artist = []
    Dtitle = []
    Category = []
    Tracks = []
# Open XML document using minidom parser
    DOMTree = xml.dom.minidom.parse("discs2.xml")
    discs1 = DOMTree.documentElement

### Get all the discs in the collection
    discs = discs1.getElementsByTagName("disc")

    a=0
    # Print detail of each movie.
    for disc in discs:
        a+=1
##   print("*****Disc*****")

        id = disc.getElementsByTagName('id')[0]
##   print("Id: %s" % id.childNodes[0].data)
        ID.append(id.childNodes[0].data)
        cid = disc.getElementsByTagName('cid')[0]
##   print ("Cid: %s" % cid.childNodes[0].data)
        cid1 = (cid.childNodes[0].data).split(',')
        CID.append(cid1)
        artist = disc.getElementsByTagName('artist')[0]
##   print ("Artist: %s" % artist.childNodes[0].data)
	
        Artist.append(prepro([artist.childNodes[0].data]))
        dtitle = disc.getElementsByTagName('dtitle')[0]
##   print ("Dtitle: %s" % dtitle.childNodes[0].data)
        Dtitle.append(prepro([dtitle.childNodes[0].data]))
        category = disc.getElementsByTagName('category')[0]
##   print ("Category: %s" % category.childNodes[0].data)
        Category.append(prepro([category.childNodes[0].data]))
        tracks = disc.getElementsByTagName("title")
        liedjes = []
        for i in range (tracks.length):
            liedjes.append(prepro([tracks.item(i).childNodes[0].data]))
##       print ("Track",i, ": %s" % tracks.item(i).childNodes[0].data)
        Tracks.append(liedjes)
    Cd = [[ID[i], CID[i], Artist[i], Dtitle[i], Category[i], Tracks[i],1] for i in range(len(ID))]
    return Cd

def cdprob(a,b):
    prob_tit = worddist(a[3],b[3])
    prob_art = worddist(a[2],b[2])
    prob = prob_tit*prob_art
    return prob

def listprob(L1):
    i = 0
    j = 1
    prob = 1
    while i<len(L1):
        for k in range(i+1,len(L1)):
            prob = prob * cdprob(CD[L1[i]],CD[L1[k]])
        i+=1
    return prob

def listprobtr(L1):
    i = 0
    j = 1
    prob = True
    while i<len(L1):
        for k in range(i+1,len(L1)):
            if cdprob(CD[L1[i]],CD[L1[k]]) <0.5:
                prob = False
    return prob

def makelist(L):
    out = []
##    out = [[list(comb)+listprob(comb)] for comb in combinations(L, 3) if listprobtr(L[:-1])]
####    out = [[([k for k,v in Counter(items1[:-1] + items2[:-1]).items() if v<2]+[k for k,v in Counter(items1[:-1] + items2[:-1]).items() if v>1])
####            , listprob([k for k,v in Counter(items1[:-1] + items2[:-1]).items() if v<2]+[k for k,v in Counter(items1[:-1] + items2[:-1]).items() if v>1])] for items2 in L for items1 in L] 
    for items1 in L:
        for items2 in L:
            L2 = items1[:-1] + items2[:-1]
            unique = [k for k,v in Counter(L2).items() if v<2]
            if len(unique) == 2:
                double = [k for k,v in Counter(L2).items() if v>1]
                if cdprob(CD[unique[0]], CD[unique[1]]) >0:
                    L1 = unique + double
                    out.append(L1 + [listprob(L1)])
    return out

def reducelist(L):
    unis = []
    out = []
    for items in L:
        count = 0
        for uni in unis:
            if set(items[0:-1]) == set(uni):
                count = 1
        if count == 0:
            unis.append(items[0:-1])
            out.append(items[0:-1] +[listprob(items[0:-1])])
    return out
                    
t = time.time()
CD = prepros()

#makes list of all numbers
onelist = range(0,len(CD))
print("onelist made")

#makes list of duos
twolist = [list(comb)+ [cdprob(CD[comb[0]], CD[comb[1]])] for comb in combinations(onelist, 2) if cdprob(CD[comb[0]], CD[comb[1]])>0]
print("twolist made")

twolist = reducelist(twolist)
print("twolist reduced")
#makes list of trios
threelist = makelist(twolist)
print("threelist made")

threelist = reducelist(threelist)
print("threelist reduced")
#makes list of four pairs
fourlist = makelist(threelist)
print("fourlist made")

fourlist = reducelist(fourlist)
print("fourlist reduced")
#makes list of five pairs
fivelist = makelist(fourlist)
print("fivelist made")

fivelist = reducelist(fivelist)
print("fivelist reduced")
#all list include the group with the final item being the probability of this group representing the same
        
print(time.time()-t)

#this function should make all possible world. But it doesnt
def findfit(CD, onelist, twolist, threelist, fourlist, fivelist, leftover,p):
    if leftover == []:
        return leftover
    print("still a leftover")
    for item in fivelist:
        print(set(item[0:-1]))
        if set(item[0:-1] ) <= set(leftover):
            p.append(item)
            leftover = [items for items in leftover if items not in item]
            p.append(findfit(CD, onelist, twolist, threelist, fourlist, fivelist, leftover,p))
        else:
            for item in fourlist:
                if set(item[0:-1] ) <= set(leftover):
                    p.append(item)
                    leftover = [items for items in leftover if items not in item]
                    p.append(findfit(CD, onelist, twolist, threelist, fourlist, fivelist, leftover,p))
                else:
                    for item in threelist:
                        if set(item[0:-1] ) <= set(leftover):
                            p.append(item)
                            leftover = [items for items in leftover if items not in item]
                            p.append(findfit(CD, onelist, twolist, threelist, fourlist, fivelist, leftover,p))
                        else:
                            for item in twolist:
                                if set(item[0:-1] ) <= set(leftover):
                                    p.append(item)
                                    leftover = [items for items in leftover if items not in item]
                                    p.append(findfit(CD, onelist, twolist, threelist, fourlist, fivelist, leftover,p))
                                else:
                                    for item in onelist:
                                        print(item)
                                        print(leftover)
                                        if set(item[0:-1] ) <= set(leftover):
                                            p.append(item)
                                            leftover = [items for items in leftover if items not in item]
                                            p.append(findfit(CD, onelist, twolist, threelist, fourlist, fivelist, leftover,p))
    return p

x = findfit(CD, onelist, twolist, threelist, fourlist, fivelist, onelist,[])
print(x)
    


