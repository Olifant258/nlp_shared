% Good practice: full denormalization.



movie_id(1).

movie_name(1,"The Namesake").

movie_year(1,2006).

movie(MID,Name,Year) :-
	movie_id(MID), movie_name(MID,Name), movie_year(MID,Year).


genre_id(1).
genre_id(2).

genre_movie(1,1).
genre_movie(2,1).

genre_genre(1,drama).
genre_genre(1,comedy).

genre(GID,MID,G) :- genre_id(GID), genre_genre(GID,G), genre_movie(GID,MID).

keyword_id(1).
keyword_id(2).
keyword_id(3).

keyword_name(1,"India").
keyword_name(2,"train").
keyword_name(3,"singer").

keyword_movie(1,1).
keyword_movie(1,2).
keyword_movie(1,3).

keyword(KID,KNM,MID) :- keyword_id(KID), keyword_name(KNM), keyword_movie(KID,MID).


location_id(1).
location_id(2).
location_id(3).

location_name(1,"Calcutta").
location_name(2,"West Bengal").
location_name(3,"India").

location_movie(1,1).
location_movie(1,2).
location_movie(1,3).

location(KID,KNM,MID) :- location_id(KID), location_name(KID,KNM), location_movie(MID,KID).

actor_id(1).
actor_id(2).
actor_id(3).
actor_id(4).
actor_id(5).
actor_id(6).

actor_name(1,"Sudipta Bhawmik").
actor_name(2,"Glenne Headley") [x1=1].
actor_name(2,"Headley,Glenne") [x1=2].
actor_name(3,"Tamal Roy Choudhury").
actor_name(4,"Amy Wright").
actor_name(5,"Sibani Biswas").
actor_name(6,"Sebastian Roche").

@uniform p(x1).

actor_movie_role(1,1,"Subroto Mesho").
actor_movie_role(2,1,"Lydia").
actor_movie_role(3,1,"Ashoke's Father").
actor_movie_role(4,1,"Pamela").
actor_movie_role(5,1,"Mrs. Mazoomdar").
actor_movie_role(6,1,"Pierre").

actor(AID,MID,Name,Role) :-
	actor_id(AID),
	actor_movie_role(AID,MID,Role),
	actor_name(AID,Name).

actorofmovie(Actor,Movie) :-
	movie(MID,Movie,Year),
	actor(AID,MID,Actor,Role).

actorroleofmovie(Name,actor,Movie) :-
   movie(MID,Movie,Year),
	actor(AID,MID,Name,Role).
actorroleofmovie(Name,role,Movie) :-
	movie(MID,Movie,Year),
	actor(AID,MID,Actor,Name).

locationofactor(Actor,Location) :-
	location(HE,Location,ID),
	actor(AID,ID,Actor,Role).
locationofactor(Actor,Location) :-
	actor(AID,ID,Actor,Role),
	location(HE,Location,ID).
	
meaning(Word, actor) :-
	actor(AID, ID, Word, Role).
meaning(Word, movie) :-
	movie(MID, Word, Year).
meaning(Word, role) :-
	actor(AID, ID, Name, Word).
meaning(Word, keyword) :-
	keyword(KID, Word, MID).
meaning(Word, location) :-
	location(KID, Word, MID).


